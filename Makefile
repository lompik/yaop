SRCDIR=src

all: clib

%.c: %.re
	re2c -W -Werror -b -i -8 --tags -o $@ $<

clib: libcorg.so
libcorg.so: $(SRCDIR)/api.c $(SRCDIR)/org_nodes.c $(SRCDIR)/scanners.c $(SRCDIR)/tostdout.c $(SRCDIR)/toast.c $(SRCDIR)/static.c $(SRCDIR)/render.c $(SRCDIR)/render_json.c
	$(CC) $(CFLAGS) -fPIC -shared $(EXTRA_WARNING_CFLAGS) -lyajl -o $@ $^
