#ifndef SCANNERS_H
#define SCANNERS_H

#include <stddef.h>

enum corg_bol_t{
    CORG_BOL_HEADLINE=0x0,
    CORG_BOL_BLOCK_BEGIN,
    CORG_BOL_BLOCK_END,
    CORG_BOL_TABLE,
    CORG_BOL_LIST_ITEM,
    CORG_BOL_FOOTNOTE_DEF,
    CORG_BOL_KEYWORD,
    CORG_BOL_KEYWORD_CUSTOM,
    CORG_BOL_KEYWORD_OX,
    CORG_BOL_FIXED_WIDTH,
    CORG_BOL_HRULE,
    CORG_BOL_NEWLINE,
    CORG_BOL_LATEXENV_BEG,
    CORG_BOL_LATEXENV_END,
    CORG_BOL_DRAWER_BEG,
    CORG_BOL_DRAWER_END,
    CORG_BOL_COMMENT,
    CORG_BOL_EOF,
    CORG_BOL_OTHER
};

struct corg_bol_ret{
    enum corg_bol_t ret;

    const unsigned char *blktype;
    const unsigned char *optkw1;
    const unsigned char *optkw2;
    ptrdiff_t pre_blank;
};

enum corg_bol_t whatbol(const unsigned char *YYCURSOR, struct corg_bol_ret *ret);

struct markers{
    const unsigned char *emphe,
        *emphb,
        *keyb,
        *keye,
        *valueb,
        *valuee,
        *bytescount,
        *rstart, // start of current match
        *start; // start of inline processing
};
#define MA_START ma.start
#define MA_EMPHE ma.emphe
#define MA_EMPHB ma.emphb
#define MA_KEYB ma.keyb
#define MA_KEYE ma.keye
#define MA_VALUEB ma.valueb
#define MA_VALUEE ma.valuee
#define MA_BYTESCOUNT ma.bytescount
#define MA_RSTART ma.rstart

int which_node_prop(const unsigned char **YYCURSOR, struct markers *res);
#endif
