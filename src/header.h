#ifndef HEADER_H
#define HEADER_H

#define YYDEBUG(state, symbol) printf("%d %c\n", state, symbol);

struct title_pointers{
    const unsigned char *st1,
        *kw1,
        *pr1,
        *ti1,
        *tg1,
        *st2,
        *kw2,
        *pr2,
        *ti2,
        *tg2;
};

#define TIST1 ti.st1
#define TIKW1 ti.kw1
#define TIPR1 ti.pr1
#define TITI1 ti.ti1
#define TITG1 ti.tg1
#define TIST2 ti.st2
#define TIKW2 ti.kw2
#define TIPR2 ti.pr2
#define TITI2 ti.ti2
#define TITG2 ti.tg2

struct ts_pointers{
    const unsigned char *kw1,
        *year1,
        *month1,
        *day1,
        *hour1,
        *min1,
        *rep1,
        *kw2,
        *year2,
        *month2,
        *day2,
        *hour2,
        *min2,
        *rep2,
        *valuee,

        *timerange_hour1,
        *timerange_min1,
        *timerange_hour2,
        *timerange_min2;
};


#define TTKW1   ts.kw1
#define TTYEAR1   ts.year1
#define TTMONTH1   ts.month1
#define TTDAY1   ts.day1
#define TTHOUR1   ts.hour1
#define TTMIN1   ts.min1
#define TTREP1   ts.rep1
#define TTKW2   ts.kw2
#define TTYEAR2   ts.year2
#define TTMONTH2   ts.month2
#define TTDAY2   ts.day2
#define TTHOUR2   ts.hour2
#define TTMIN2   ts.min2
#define TTREP2   ts.rep2
#define TTVE ts.valuee
#define TTTRH1 ts.timerange_hour1
#define TTTRM1 ts.timerange_min1
#define TTTRH2 ts.timerange_hour2
#define TTTRM2 ts.timerange_min2

struct markers_link{
    const unsigned char *pathe, *pathb, *typeb, *typee, *descriptionb, *descriptione, *valueb, *valuee;
};

#define MLKPATHE mlk.pathe
#define MLKPATHB mlk.pathb
#define MLKTYPEB mlk.typeb
#define MLKTYPEE mlk.typee
#define MLKDESCRIPTIONB mlk.descriptionb
#define MLKDESCRIPTIONE mlk.descriptione
#define MLKVALUEB mlk.valueb
#define MLKVALUEE mlk.valuee

struct mtags_t{
    int pred;
    const unsigned char *tag;
};
#include <stdlib.h>
struct mtagsv_t {
    size_t n,m; struct mtags_t *a;
};
#define mtv_size(v) ((v).n)
#define mtv_A(v, i) ((v).a[(i)])

typedef int (*process_planning)(void *header_data, struct ts_pointers ts);
typedef int (*process_header)(void *header_data, struct title_pointers ti);
typedef int (*process_prop_drawer)(void *header_data, const unsigned char *propd_beg, const unsigned char *propd_end);
typedef int (*process_options)(void *header_data, const unsigned char *YYSCURSOR, const unsigned char *optkw1, const unsigned char *optkw2);

#endif
