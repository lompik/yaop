#include "org_nodes.h"

int total_num = 0;


void *arena_malloc(omt_arena *arena, size_t size) {
    omt_arena_chunk *current_chunk = arena->head;
    if (arena->allocation_ptr >= current_chunk->data + CHUNK_SIZE - size) {
        omt_arena_chunk *new_chunk = malloc(sizeof(omt_arena_chunk));
        if(new_chunk==NULL){
            fprintf(stderr, "Not enough memory. Aborting");
            abort();
        }
        memset(new_chunk, 0, sizeof(omt_arena_chunk));
        // debug("Allocating new arena chunk @%p\n", new_chunk);
        new_chunk->next = current_chunk;
        arena->head = new_chunk;
        arena->allocation_ptr = new_chunk->data;
    }
    void *obj = arena->allocation_ptr;
    arena->allocation_ptr += (size + ALIGNMENT - 1) & ~(ALIGNMENT - 1);
    return obj;
}

void *mmalloc(struct corg_ctx *ctx, size_t size){
    void *new =0;
    if(USE_ARENA){
        new = arena_malloc(&ctx->node_pool, size);
    }
    else{
        new = calloc(size, 1);
        if(new==NULL){
            fprintf(stderr, "Not enough memory. Aborting");
            abort();
        }
    }
    return new;
}

struct node node_zero = {0};

#define m_o_f(ptr, size) if(toalloc==TRUE){ ptr = mmalloc(ctx, size);    } else if(! USE_ARENA){            free(ptr);        }
static inline void plist_onoff(struct node *new, int toalloc, struct corg_ctx *ctx){

    switch(new->type) {
    case onHeadline: {
        m_o_f(new->plist.headline, sizeof(struct corg_headline));
        break;
    }
    case onTimestamp: {
        m_o_f(new->plist.timestamp, sizeof(struct corg_timestamp));
        break;
    }
    case onExport_Block:
    case onSrc_Block:
        m_o_f(new->plist.emph, sizeof(struct corg_se_block));
        break;
    case onBold:
    case onItalic:
    case onVerbatim:
    case onStrike_Through:
    case onUnderline:
        m_o_f(new->plist.emph, sizeof(struct corg_plist_emph));
        break;
    case onLink:
        m_o_f(new->plist.link, sizeof(struct corg_link));
        break;
    case onKeyword:
        m_o_f(new->plist.keyword, sizeof(struct corg_keyword));
        break;
    case onLatex_Environment:
        m_o_f(new->plist.latex_env, sizeof(struct corg_ltxenv));
        break;
    case onExport_Snippet:
        m_o_f(new->plist.exp_snip, sizeof(struct corg_export_snippet));
        break;
    case onNode_Property:
    case onMacro:
        m_o_f(new->plist.kv, sizeof(struct corg_kv));
        break;
    case onFootnote_Definition:
    case onFootnote_Reference:{
        m_o_f(new->plist.fn, sizeof(struct corg_footnote));
        break;
    }
    case onItem:{
        m_o_f(new->plist.li, sizeof(struct corg_list_item));
        break;
    }
    default:
        break;
    }
#undef m_o_f
}


struct node *new_node(enum org_node_t type, struct corg_ctx *ctx) {
    struct node *new=0;

    new = mmalloc(ctx, sizeof(struct node));
    new->type=type;
    plist_onoff(new, TRUE, ctx);

    if( ! USE_ARENA){
        new->first_child=new->last_child=new->prev=new->next=new->parent=0;
        new->text.position=0;
    }
    if(ctx) {
        if(ctx->last_by_type[type]!=NULL){
            new->prev_by_type=ctx->last_by_type[type];
            ctx->last_by_type[type]->next_by_type=new;
        }
        ctx->last_by_type[type] = new;
        new->line = ctx->line;
    }
    assert(new->next == NULL);
    return new;
}

void free_nodes(struct node *nodes, struct corg_ctx *ctx){
    if (USE_ARENA){
        omt_arena_chunk *iter = ctx->node_pool.head;
        while(iter!=NULL){

            omt_arena_chunk *next = iter->next;
            free(iter);
            iter = next;
        }
    }
    else{
        struct node  *iter = nodes;
        while(iter != NULL){
            if (iter->last_child) {
                // Splice children into list
                iter->last_child->next = iter->next;
                iter->next = iter->first_child;
            }
            struct node *next = iter->next;
            plist_onoff(iter, FALSE, ctx);
            free(iter);
            iter = next;

        }
    }
}

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

void print_ts(struct timestamp ts){
    printf("%s %.*s%s\n", KRED, (int)ts.raw_value.len, ts.raw_value.position, KNRM);
}

void print_color_string(const char *color, struct string *s){
    printf("%s%.*s%s", color, (int)s->len, s->position, KNRM);
}

static void print_block(struct string *text) {
    const unsigned char *iter;
    for(iter=text->position; iter<text->position+text->len; iter++){
        assert(*iter!=0);
        switch (*iter) {

        case '\n':
            printf("\\n");
            break;
        case '\r':
            printf("\\r");
            break;
        case '\t':
            printf("\\t");
            break;
        default:
            if ((*iter < 0x20) || (*iter > 0x7f)) {
                printf("\\%03o", (unsigned char)*iter);
            } else {
                printf("%c", *iter);
            }
            break;
        }
    }
}


void print_node(struct node *nodes, int indent){
    struct node  *iter = nodes;
    printf("%*c %s%10s%s ", indent * 2, '@', KGRN, org_node_t_str[nodes->type], KNRM);
    switch(nodes->type){
    case onLeaf: {
        if(iter->text.position && iter->text.len >0){
            enum org_node_t t = iter->parent->type;
            char c = (
                (t == onBold) ||
                (t == onItalic) ||
                (t == onStrike_Through) ||
                (t == onVerbatim) ||
                (t == onUnderline)) ? iter->parent->plist.emph->type :' ';

            printf("%c %.*s", c , (int)iter->text.len, iter->text.position);
        }
        break;
    }
    case onSrc_Block:
        print_block(&nodes->text);
        break;
    case onLink:
        print_color_string(KMAG, &(nodes->text));
        break;
    case onHeadline:{
        struct corg_headline hd = *nodes->plist.headline;
        if(hd.todo_keyword.len > 0)  {
            printf("%stodo:%s  %.*s · ", KYEL, KNRM, (int)hd.todo_keyword.len, hd.todo_keyword.position);
        }
        if(hd.priority)  {
            printf("%spriority:%s %c · ", KMAG, KNRM, hd.priority);
        }

        if(hd.tags.len > 0)  {
            printf("%stags:%s  %.*s · ", KBLU, KNRM, (int)hd.tags.len, hd.tags.position);
        }
        printf("%stitle:%s %.*s  ░ ", KYEL, KNRM, (int)hd.raw_value.len, hd.raw_value.position);

        if(hd.planning[HEADLINE_PLANNING_DEADLINE].year_start != 0){
            printf("%sDEADLINE: %s", KYEL, KNRM);
            print_ts(hd.planning[HEADLINE_PLANNING_DEADLINE]);
        }
        if(hd.planning[HEADLINE_PLANNING_CLOSED].year_start != 0){
            printf("%sCLOSED: %s", KYEL, KNRM);
            print_ts(hd.planning[HEADLINE_PLANNING_CLOSED]);
        }
        if(hd.planning[HEADLINE_PLANNING_SCHEDULED].year_start != 0){
            printf("%sSCHEDULED: %s", KYEL, KNRM);
            print_ts(hd.planning[HEADLINE_PLANNING_SCHEDULED]);
        }
        break;
    }
    case onTimestamp:{
        print_ts(nodes->plist.timestamp->ts);
        break;
    }
    default:
        break;
    }
    printf("\n");
    if(iter->first_child){
        print_node(iter->first_child, indent+1);
    }

    if(iter->next){
        print_node(iter->next, indent);
    }
}

int corg_node_check(struct node *nodes) {
  struct node *cur;
  int errors = 0;

  if (!nodes) {
    return 0;
  }

  cur = nodes;
  for (;;) {
    if (cur->first_child) {
      if (cur->first_child->prev != NULL) {
        log_err("%s", "prev");
        cur->first_child->prev = NULL;
        ++errors;
      }
      if (cur->first_child->parent != cur) {
        log_err("%s", "parent");
        cur->first_child->parent = cur;
        ++errors;
      }
      cur = cur->first_child;
      continue;
    }

  next_sibling:
    if (cur == nodes) {
      break;
    }
    if (cur->next) {
      if (cur->next->prev != cur) {
        log_err("%s", "prev");
        cur->next->prev = cur;
        ++errors;
      }
      if (cur->next->parent != cur->parent) {
          log_err("%s", "parent");
        cur->next->parent = cur->parent;
        ++errors;
      }
      cur = cur->next;
      continue;
    }

    if (cur->parent->last_child != cur) {
      log_err("%s", "last_child");
      cur->parent->last_child = cur;
      ++errors;
    }
    cur = cur->parent;
    goto next_sibling;
  }
  assert(errors==0);
  return errors;
}

struct node *new_leaf_node_with_text(const unsigned char *position, int len, struct corg_ctx *ctx){
    struct node *tleaf = new_node(onLeaf, ctx);
    tleaf->text.position = position;
    tleaf->text.len = len; total_num+=len;
    assert(tleaf->text.len>=0);
    return tleaf;
}

void insert_last(struct node *parent, struct node *child){
    child->parent = parent;

    if(parent->last_child){
        assert(parent->last_child->next == 0);
        parent->last_child->next = child;
        child->prev = parent->last_child;
        parent->last_child = child;
    } else {
        parent->last_child = child;
        parent->first_child = child;
        child->prev = NULL;
        child->next = NULL;
    }

#ifdef BOOKKEEPING_NODES

#endif

}

void insert_last_headline(struct corg_ctx *ctx, struct node *child){
    struct node *lh = ctx->last_by_type[onHeadline];
    if (lh == NULL){
        lh = ctx->document;
    } else if(lh==child){
        lh = lh->prev_by_type;
        if(lh==NULL){
            lh = ctx->document;
        }
    }
    insert_last(lh, child);
}

void insert_as_parent_at(struct node *at, struct node *node){

    node->parent = at->parent;
    at->parent = node;
    struct node *iter=at;
    node->last_child=iter;
    while((iter = iter->next)) {
        iter->parent = node;
        node->last_child=iter;
    }
    node->first_child=at;

    node->parent->last_child = node;

    if(node->parent->first_child == at)
        node->parent->first_child = node;

    node->prev = at->prev;
    if(node->prev){
        node->prev->next = node;
    }

    at->prev=0;

#ifdef BOOKKEEPING_NODES

#endif

}

void print_node_counts(struct corg_ctx *ctx){
    for(int i=onFIRST+1; i<onLAST; i++){
        int count=0;
        if(ctx->last_by_type[i]){
            struct node *n=ctx->last_by_type[i];
            while(n) {
                count++;
                n=n->prev_by_type;
            }
        }
        if(count!=0) printf("%15s %d\n", org_node_t_str[i], count);
    }
}

struct node *corg_ctx_document(struct corg_ctx *ctx){
    if(ctx)
        return ctx->document;
    else return NULL;
}

const char *corg_ctx_input_str(struct corg_ctx *ctx){
    if(ctx)
        return (const char *)ctx->input_string;
    else return NULL;
}

void corg_free_ctx(struct corg_ctx *ctx){
    if(ctx == NULL) return;
    if(ctx->document){
        free_nodes(ctx->document, ctx);
        ctx->document = NULL;
    }
    free(ctx);
}

struct node *corg_ctx_last_by_type(struct corg_ctx *ctx, enum org_node_t ntype){
    if(ctx && ctx->document && ntype> onFIRST && ntype<onLAST)
        return ctx->last_by_type[ntype];
    return NULL;
}

struct node *corg_ctx_first_by_type(struct corg_ctx *ctx, enum org_node_t ntype){
    struct node *it = corg_ctx_last_by_type(ctx, ntype);
    if(it == NULL){
        return NULL;
    }

    while(it){
        if(it->prev_by_type)
            it=it->prev_by_type;
        else
            return it;
    };
    return NULL;
}

struct sv stringpiece_slice(struct corg_ctx *ctx, struct string *s){
    if (s->position && s->len !=0){
        struct sv res={(const char *)s->position-corg_ctx_input_str(ctx),
                       s->len+(const char *)s->position-corg_ctx_input_str(ctx)};
        return(res);
    }
    struct sv sv0 = {0};
    return sv0;
}

const int org_node_t_int[] = {
    onFIRST,
    onCenter_Block,
    onDrawer,
    onDynamic_Block,
    onFootnote_Definition,
    onHeadline,
    onInlinetask,
    onItem,
    onPlain_List,
    onProperty_Drawer,
    onQuote_Block,
    onSection,
    onSpecial_Block,
    onBabel_Call,
    onClock,
    onComment,
    onComment_Block,
    onDiary_Sexp,
    onExample_Block,
    onExport_Block,
    onFixed_width,
    onHorizontal_Rule,
    onKeyword,
    onLatex_Environment,
    onNode_Property,
    onParagraph,
    onPlanning,
    onSrc_Block,
    onTable,
    onTable_Row,
    onVerse_Block,
    onBold,
    onCode,
    onEntity,
    onExport_Snippet,
    onFootnote_Reference,
    onInline_Babel_Call,
    onInline_Src_Block,
    onItalic,
    onLatex_Fragment,
    onLine_Break,
    onLink,
    onMacro,
    onRadio_target,
    onStatistics_Cookie,
    onStrike_Through,
    onSubscript,
    onSuperscript,
    onTable_Cell,
    onTarget,
    onTimestamp,
    onUnderline,
    onVerbatim,

    onLeaf,
    onDocument,

    onLAST
};


const char *org_node_t_str[] = {
    "onFIRST",
    "onCenter_Block",
    "onDrawer",
    "onDynamic_Block",
    "onFootnote_Definition",
    "onHeadline",
    "onInlinetask",
    "onItem",
    "onPlain_List",
    "onProperty_Drawer",
    "onQuote_Block",
    "onSection",
    "onSpecial_Block",
    "onBabel_Call",
    "onClock",
    "onComment",
    "onComment_Block",
    "onDiary_Sexp",
    "onExample_Block",
    "onExport_Block",
    "onFixed_width",
    "onHorizontal_Rule",
    "onKeyword",
    "onLatex_Environment",
    "onNode_Property",
    "onParagraph",
    "onPlanning",
    "onSrc_Block",
    "onTable",
    "onTable_Row",
    "onVerse_Block",
    "onBold",
    "onCode",
    "onEntity",
    "onExport_Snippet",
    "onFootnote_Reference",
    "onInline_Babel_Call",
    "onInline_Src_Block",
    "onItalic",
    "onLatex_Fragment",
    "onLine_Break",
    "onLink",
    "onMacro",
    "onRadio_target",
    "onStatistics_Cookie",
    "onStrike_Through",
    "onSubscript",
    "onSuperscript",
    "onTable_Cell",
    "onTarget",
    "onTimestamp",
    "onUnderline",
    "onVerbatim",

    "onLeaf",
    "onDocument",

    "onLAST"
};

int ts_rpt_wrn_unit_to_seconds(char u){
    switch(u){
    case 'h': return 60*60;
    case 'd': return 60*60*24;
    case 'w': return 60*60*24*7;
    case 'm': return 60*60*24*30;
    case 'y': return 60*60*24*365;
    }
    return 0;
}
