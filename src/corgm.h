#ifndef CORGM_H
#define CORGM_H

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif


//s

enum org_node_t{
    onFIRST = 0,
    onCenter_Block,
    onDrawer,
    onDynamic_Block,
    onFootnote_Definition,
    onHeadline,
    onInlinetask,
    onItem,
    onPlain_List,
    onProperty_Drawer,
    onQuote_Block,
    onSection,
    onSpecial_Block,
    onBabel_Call,
    onClock,
    onComment,
    onComment_Block,
    onDiary_Sexp,
    onExample_Block,
    onExport_Block,
    onFixed_width,
    onHorizontal_Rule,
    onKeyword,
    onLatex_Environment,
    onNode_Property,
    onParagraph,
    onPlanning,
    onSrc_Block,
    onTable,
    onTable_Row,
    onVerse_Block,
    onBold,
    onCode,
    onEntity,
    onExport_Snippet,
    onFootnote_Reference,
    onInline_Babel_Call,
    onInline_Src_Block,
    onItalic,
    onLatex_Fragment,
    onLine_Break,
    onLink,
    onMacro,
    onRadio_target,
    onStatistics_Cookie,
    onStrike_Through,
    onSubscript,
    onSuperscript,
    onTable_Cell,
    onTarget,
    onTimestamp,
    onUnderline,
    onVerbatim,

    onLeaf,
    onDocument,

    onLAST
};

extern const char *org_node_t_str[];
extern const int org_node_t_int[];


struct string{
    const unsigned char *position;
    ptrdiff_t len;
};

struct corg_plist_emph {
    char type;
};

struct corg_common{
    int begin, end, pre_blank, pos_blank, contents_begin, contents_end;
};

enum ts_repeater_type{
    TS_RPT_NONE=0x0,
    TS_RPT_CATCHUP=0x1,
    TS_RPT_RESTART=0x2,
    TS_RPT_CUMULATE=0x3,
};

enum ts_warning_type{
    TS_WRN_NONE=0x0,
    TS_WRN_FIRST=0x1,
    TS_WRN_ALL=0x2
};

struct timestamp {
    int active ;
    struct string raw_value ;
    int year_start;
    int month_start;
    int day_start;
    int hour_start;
    int minute_start;
    int year_end;
    int month_end;
    int day_end;
    int hour_end;
    int minute_end;
    enum ts_warning_type warning_type;
    int warning_value;
    char warning_unit;
    enum ts_repeater_type repeater_type;
    int repeater_value;
    char repeater_unit;
};

struct corg_timestamp{
    struct corg_common bytes;
    struct timestamp ts;
};

#define HEADLINE_PLANNING_SCHEDULED 0
#define HEADLINE_PLANNING_DEADLINE 1
#define HEADLINE_PLANNING_CLOSED 2

struct corg_headline{
    struct corg_common bytes;
    int level;
    int line;
    struct string tags;
    struct string raw_value;
    struct string todo_keyword;
    struct node *title;
    char priority;
    struct timestamp planning[3];
};


enum corg_link_format_t{
    olfBracket=0,
    olfAngle,
    olfPlain,
};

enum corg_link_type_t{
    oltFuzzy=0,
    oltCoderef,
    oltCustom_id,
    oltSupported
};

enum corg_hkeys_t{
    kFIRST=0,
    kARCHIVE,
    kASCII,
    kATTR_CENTER,
    kATTR_COMMENT,
    kATTR_EXAMPLE,
    kATTR_EXPORT,
    kATTR_LATEX,
    kATTR_QUOTE,
    kATTR_SRC,
    kATTR_VERSE,
    kAUTHOR,
    kBEGIN_CENTER,
    kBEGIN_COMMENT,
    kBEGIN_EXAMPLE,
    kBEGIN_EXPORT,
    kBEGIN_QUOTE,
    kBEGIN_SRC,
    kBEGIN_VERSE,
    kBIND,
    kCAPTION,
    kCATEGORY,
    kCOLUMNS,
    kCREATOR,
    kDATA,
    kDATE,
    kDESCRIPTION,
    kDRAWERS,
    kEMAIL,
    kEND_CENTER,
    kEND_COMMENT,
    kEND_EXAMPLE,
    kEND_EXPORT,
    kEND_QUOTE,
    kEND_SRC,
    kEND_VERSE,
    kEXCLUDE_TAGS,
    kFILETAGS,
    kHEADER,
    kHEADERS,
    kHTML,
    kHTML_CONTAINER,
    kHTML_DOCTYPE,
    kHTML_HEAD,
    kHTML_HEAD_EXTRA,
    kHTML_LINK_HOME,
    kHTML_LINK_UP,
    kHTML_MATHJAX,
    kICALENDAR,
    kICALENDAR_EXCLUDE_TAGS,
    kINCLUDE,
    kINDEX,
    kINFOJS_OPT,
    kKEYWORDS,
    kLABEL,
    kLANGUAGE,
    kLATEX,
    kLATEX_CLASS,
    kLATEX_CLASS_OPTIONS,
    kLATEX_COMPILER,
    kLATEX_HEADER,
    kLATEX_HEADER_EXTRA,
    kMACRO,
    kNAME,
    kODT,
    kODT_STYLES_FILE,
    kOPTIONS,
    kPLOT,
    kPRIORITIES,
    kPROPERTY,
    kRESNAME,
    kRESULT,
    kRESULTS,
    kSELECT_TAGS,
    kSEQ_TODO,
    kSETUPFILE,
    kSOURCE,
    kSRCNAME,
    kSTARTUP,
    kSUBTITLE,
    kTAGS,
    kTBLNAME,
    kTITLE,
    kTODO,
    kTYP_TODO,
    kCUSTOM_OX
};

enum onp_props {
    onpID,
    onpALLTAGS,
    onpARCHIVE,
    onpBLOCKED,
    onpCATEGORY,
    onpCLOCK_MODELINE_TOTAL,
    onpCLOCKSUM,
    onpCLOCKSUM_T,
    onpCLOSED,
    onpCOLUMNS,
    onpCOOKIE_DATA,
    onpCUSTOM_ID,
    onpDEADLINE,
    onpDESCRIPTION,
    onpEFFORT,
    onpEXPORT_AUTHOR,
    onpEXPORT_DATE,
    onpEXPORT_FILE_NAME,
    onpEXPORT_OPTIONS,
    onpEXPORT_TEXT,
    onpEXPORT_TITLE,
    onpFILE,
    onpHTML_CONTAINER_CLASS,
    onpITEM,
    onpLOCATION,
    onpLOGGING,
    onpLOG_INTO_DRAWER,
    onpNOBLOCKING,
    onpORDERED,
    onpPRIORITY,
    onpREPEAT_TO_STATE,
    onpSCHEDULED,
    onpSTYLE,
    onpSUMMARY,
    onpTABLE_EXPORT_FILE,
    onpTABLE_EXPORT_FORMAT,
    onpTAGS,
    onpTIMESTAMP,
    onpTIMESTAMP_IA,
    onpTODO,
    onpUNNUMBERED,
    onpVISIBILITY,
    onp_CUSTOM
};

struct corg_keyword{
    struct corg_common bytes;
    enum corg_hkeys_t keyt;
    struct string key;
    struct string value;
};

struct corg_link{
    struct corg_common bytes;
    enum corg_link_format_t format;
    enum corg_link_type_t typet;
    struct string raw_value;
    struct string path;
    struct string type;
    struct string application;
    struct string search_option;
    struct node *descrition;
};

struct corg_ltxenv{
    struct string environment;
};

struct corg_se_block{
    struct corg_common bytes;
    struct string lang;
    struct string switches;
    struct string headers;
};

enum corg_list_checkbox{
    CORG_LIST_CHECKBOX_ON,
    CORG_LIST_CHECKBOX_OFF,
    CORG_LIST_CHECKBOX_TRANS,
    CORG_LIST_CHECKBOX_NIL
};

struct corg_list_item{
    enum corg_list_checkbox checkbox;
    struct string tag;
    struct string bullet;
    ptrdiff_t pre_blank;
    int counter;
};

struct corg_kv{
    struct corg_common bytes;
    struct string key;
    struct string value;
    int keyt;
};

enum efootnote_t {
    FOOTNOTE_INLINE,
    FOOTNOTE_STANDARD,
    FOOTNOTE_DEFINITION
};

struct corg_footnote{
    struct corg_common bytes;
    enum efootnote_t type;
    struct string label;
    struct string value;
};

struct corg_export_snippet{
    struct corg_common bytes;
    struct string back_end;
    struct string value;
};

struct node{
    enum org_node_t type;
    struct node *parent;
    struct node *next, *prev;
    struct node *first_child, *last_child;

    struct node *next_by_type, *prev_by_type;
    unsigned int line;
    struct string text;
    union {
        struct corg_plist_emph *emph;
        struct corg_headline *headline;
        struct corg_timestamp *timestamp;
        struct corg_link *link;
        struct corg_keyword *keyword;
        struct corg_se_block *se_blocks;
        struct corg_ltxenv *latex_env;
        struct corg_export_snippet *exp_snip;
        struct corg_kv *kv;
        struct corg_footnote *fn;
        struct corg_list_item *li;
    } plist;
};

//e

struct corg_common get_export_snippet_bytes(struct corg_export_snippet *in);
struct corg_common get_footnote_bytes(struct corg_footnote *in);
struct corg_common get_headline_bytes(struct corg_headline *in);
struct corg_common get_keyword_bytes(struct corg_keyword *in);
struct corg_common get_kv_bytes(struct corg_kv *in);
struct corg_common get_link_bytes(struct corg_link *in);
enum corg_list_checkbox get_list_item_checkbox(struct corg_list_item *in);
struct string get_ltxenv_environment(struct corg_ltxenv *in);
struct corg_export_snippet *get_node_export_snippet(struct node *node);
struct node *get_node_first_child(struct node *node);
struct corg_footnote *get_node_footnote(struct node *node);
struct corg_headline *get_node_headline(struct node *node);
struct corg_keyword *get_node_keyword(struct node *node);
struct corg_kv *get_node_kv(struct node *node);
struct node *get_node_last_child(struct node *node);
struct corg_link *get_node_link(struct node *node);
struct corg_list_item *get_node_list_item(struct node *node);
struct corg_ltxenv *get_node_ltxenv(struct node *node);
struct node *get_node_next(struct node *node);
struct node *get_node_next_by_type(struct node *node);
struct node *get_node_parent(struct node *node);
struct corg_plist_emph *get_node_plist_emph(struct node *node);
struct node *get_node_prev(struct node *node);
struct node *get_node_prev_by_type(struct node *node);
struct corg_se_block *get_node_se_block(struct node *node);
struct corg_timestamp *get_node_timestamp(struct node *node);
char get_plist_emph_type(struct corg_plist_emph *in);
struct corg_common get_se_block_bytes(struct corg_se_block *in);
struct corg_common get_timestamp_bytes(struct corg_timestamp *in);


/**************************************************************************/
/* Context                                                                */
/**************************************************************************/

struct corg_ctx;
struct node *corg_ctx_document(struct corg_ctx *ctx);
const char *corg_ctx_input_str(struct corg_ctx *ctx);
struct corg_ctx *corg_parse_to_ast(char* buf);
void corg_free_ctx(struct corg_ctx *ctx);
struct node *corg_ctx_first_by_type(struct corg_ctx *ctx, enum org_node_t ntype);
void free_nodes(struct node *nodes, struct corg_ctx *ctx);

//string
struct sv {
    long int beg, end;
};
struct sv stringpiece_slice(struct corg_ctx *ctx, struct string *s);

#ifdef __cplusplus
}
#endif

#endif
