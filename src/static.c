#include "org_nodes.h"
#include <ctype.h>

const struct org_block_types begin_blocks[] = {
    {"CENTER", "center", ARRAY_SIZE("center"), onCenter_Block},
    {"COMMENT", "comment", ARRAY_SIZE("comment"), onComment_Block},
    {"EXAMPLE", "example", ARRAY_SIZE("example"), onExample_Block},
    {"EXPORT", "export", ARRAY_SIZE("export"), onExport_Block},
    {"QUOTE", "quote", ARRAY_SIZE("quote"), onQuote_Block},
    {"SRC", "src", ARRAY_SIZE("src"), onSrc_Block},
    {"VERSE", "verse", ARRAY_SIZE("verse"), onVerse_Block},
    {0,0,0,0}};

int node_t_to_obt(enum org_node_t type){
    for(size_t i=0; i<ARRAY_SIZE(begin_blocks)-1; i++)
        if(begin_blocks[i].node==type)
            return i;
    abort();
    return -1;
}

struct keywords keyword_map[][8] = {
        {{"BIND:", kBIND}, {"DATE:", kDATE}, {"TAGS:", kTAGS}, {"TODO:", kTODO}, {NULL, 0}},  //5
        {{"EMAIL:", kEMAIL}, {"INDEX:", kINDEX}, {"MACRO:", kMACRO}, {"TITLE:", kTITLE}, {NULL, 0}}, //6
        {{"AUTHOR:", kAUTHOR}, {NULL, 0}}, //7
        {{"ARCHIVE:", kARCHIVE}, {"COLUMNS:", kCOLUMNS}, {"CREATOR:", kCREATOR}, {"DRAWERS:", kDRAWERS}, {"INCLUDE:", kINCLUDE},{"OPTIONS:", kOPTIONS},{"STARTUP:", kSTARTUP},{NULL, 0}},
        {{"CATEGORY:", kCATEGORY},{"FILETAGS:", kFILETAGS},{"KEYWORDS:", kKEYWORDS},{"LANGUAGE:", kLANGUAGE},{"PROPERTY:", kPROPERTY},{"SEQ_TODO:", kSEQ_TODO},{"TYP_TODO:", kTYP_TODO},{NULL, 0}}, //9
        {{"SETUPFILE:", kSETUPFILE},{NULL, 0}}, //10
        {{"PRIORITIES:", kPRIORITIES},{NULL, 0}}, //11
        {{"DESCRIPTION:", kDESCRIPTION},{"SELECT_TAGS:", kSELECT_TAGS},{NULL, 0}}, //12
        {{"EXCLUDE_TAGS:", kEXCLUDE_TAGS},{NULL, 0}} //13
};

enum corg_hkeys_t wkey(struct string keyword){
    int lentopos[] = {2, 0, 0, 3, 0, 0, 0, 0, 0};

    if(keyword.len==7) return kAUTHOR;
    else if(keyword.len==10) return kSETUPFILE;
    else if(keyword.len==11) return kPRIORITIES;
    else if(keyword.len==13) return kEXCLUDE_TAGS;

    size_t len_index = keyword.len - 5;
    int char_index = lentopos[len_index];
    char k = toupper(keyword.position[char_index]);
    for(size_t i=0; i<ARRAY_SIZE(keyword_map[len_index]); i++)
        if(keyword_map[len_index][i].key[char_index] == k)
            return keyword_map[len_index][i].hkey;

    abort();
    return kFIRST;
}


struct keywords all_keywords_3[] = {
    {"ODT", kODT}};

struct keywords all_keywords_4[] = {
    {"BIND", kBIND},
    {"DATA", kDATA},
    {"DATE", kDATE},
    {"HTML", kHTML},
    {"NAME", kNAME},
    {"PLOT", kPLOT},
    {"TAGS", kTAGS},
    {"TODO", kTODO}};

struct keywords all_keywords_5[] = {
    {"ASCII", kASCII},
    {"EMAIL", kEMAIL},
    {"INDEX", kINDEX},
    {"LABEL", kLABEL},
    {"LATEX", kLATEX},
    {"MACRO", kMACRO},
    {"TITLE", kTITLE}};

struct keywords all_keywords_6[] = {
    {"AUTHOR", kAUTHOR},
    {"HEADER", kHEADER},
    {"RESULT", kRESULT},
    {"SOURCE", kSOURCE}};

struct keywords all_keywords_7[] = {
    {"ARCHIVE", kARCHIVE},
    {"CAPTION", kCAPTION},
    {"COLUMNS", kCOLUMNS},
    {"CREATOR", kCREATOR},
    {"DRAWERS", kDRAWERS},
    {"END_SRC", kEND_SRC},
    {"HEADERS", kHEADERS},
    {"INCLUDE", kINCLUDE},
    {"OPTIONS", kOPTIONS},
    {"RESNAME", kRESNAME},
    {"RESULTS", kRESULTS},
    {"SRCNAME", kSRCNAME},
    {"STARTUP", kSTARTUP},
    {"TBLNAME", kTBLNAME}};

struct keywords all_keywords_8[] = {
    {"ATTR_SRC", kATTR_SRC},
    {"CATEGORY", kCATEGORY},
    {"FILETAGS", kFILETAGS},
    {"KEYWORDS", kKEYWORDS},
    {"LANGUAGE", kLANGUAGE},
    {"PROPERTY", kPROPERTY},
    {"SEQ_TODO", kSEQ_TODO},
    {"SUBTITLE", kSUBTITLE},
    {"TYP_TODO", kTYP_TODO}};

struct keywords all_keywords_9[] = {
    {"BEGIN_SRC", kBEGIN_SRC},
    {"CUSTOM_OX", kCUSTOM_OX},
    {"END_QUOTE", kEND_QUOTE},
    {"END_VERSE", kEND_VERSE},
    {"HTML_HEAD", kHTML_HEAD},
    {"ICALENDAR", kICALENDAR},
    {"SETUPFILE", kSETUPFILE}};

struct keywords all_keywords_10[] = {
    {"ATTR_LATEX", kATTR_LATEX},
    {"ATTR_QUOTE", kATTR_QUOTE},
    {"ATTR_VERSE", kATTR_VERSE},
    {"END_CENTER", kEND_CENTER},
    {"END_EXPORT", kEND_EXPORT},
    {"INFOJS_OPT", kINFOJS_OPT},
    {"PRIORITIES", kPRIORITIES}};

struct keywords all_keywords_11[] = {
    {"ATTR_CENTER", kATTR_CENTER},
    {"ATTR_EXPORT", kATTR_EXPORT},
    {"BEGIN_QUOTE", kBEGIN_QUOTE},
    {"BEGIN_VERSE", kBEGIN_VERSE},
    {"DESCRIPTION", kDESCRIPTION},
    {"END_COMMENT", kEND_COMMENT},
    {"END_EXAMPLE", kEND_EXAMPLE},
    {"LATEX_CLASS", kLATEX_CLASS},
    {"SELECT_TAGS", kSELECT_TAGS}};

struct keywords all_keywords_12[] = {
    {"ATTR_COMMENT", kATTR_COMMENT},
    {"ATTR_EXAMPLE", kATTR_EXAMPLE},
    {"BEGIN_CENTER", kBEGIN_CENTER},
    {"BEGIN_EXPORT", kBEGIN_EXPORT},
    {"EXCLUDE_TAGS", kEXCLUDE_TAGS},
    {"HTML_DOCTYPE", kHTML_DOCTYPE},
    {"HTML_LINK_UP", kHTML_LINK_UP},
    {"HTML_MATHJAX", kHTML_MATHJAX},
    {"LATEX_HEADER", kLATEX_HEADER}};

struct keywords all_keywords_13[] = {
    {"BEGIN_COMMENT", kBEGIN_COMMENT},
    {"BEGIN_EXAMPLE", kBEGIN_EXAMPLE}};

struct keywords all_keywords_14[] = {
    {"HTML_CONTAINER", kHTML_CONTAINER},
    {"HTML_LINK_HOME", kHTML_LINK_HOME},
    {"LATEX_COMPILER", kLATEX_COMPILER}};

struct keywords all_keywords_15[] = {
    {"HTML_HEAD_EXTRA", kHTML_HEAD_EXTRA},
    {"ODT_STYLES_FILE", kODT_STYLES_FILE}};

struct keywords all_keywords_16[] = { {0} };

struct keywords all_keywords_17[] = { {0} };

struct keywords all_keywords_18[] = {
    {"LATEX_HEADER_EXTRA", kLATEX_HEADER_EXTRA}};

struct keywords all_keywords_19[] = {
    {"LATEX_CLASS_OPTIONS", kLATEX_CLASS_OPTIONS}};

struct keywords all_keywords_20[] = { {0} };

struct keywords all_keywords_21[] = { {0} };

struct keywords all_keywords_22[] = {
    {"ICALENDAR_EXCLUDE_TAGS", kICALENDAR_EXCLUDE_TAGS}};


struct keywords *all_keywords[]= { all_keywords_3, all_keywords_4, all_keywords_5, all_keywords_6, all_keywords_7, all_keywords_8, all_keywords_9, all_keywords_10, all_keywords_11, all_keywords_12, all_keywords_13, all_keywords_14, all_keywords_15, all_keywords_16, all_keywords_17, all_keywords_18, all_keywords_19, all_keywords_20, all_keywords_21, all_keywords_22 };

#include <strings.h>
enum corg_hkeys_t recon_keyword(struct string keyword){

    if(keyword.len==3) return(kODT);
    if(keyword.len==18) return(kLATEX_HEADER_EXTRA);
    if(keyword.len==19) return(kLATEX_CLASS_OPTIONS);
    if(keyword.len==22) return(kICALENDAR_EXCLUDE_TAGS);

    if (keyword.len > 22 || keyword.len  < 3) {
        return kFIRST;

    }
    size_t len_index = keyword.len - 3;

    //char k = toupper(keyword.position[char_index]);
    struct keywords *map = keyword_map[len_index];
    for(size_t i=0; i<=ARRAY_SIZE(keyword_map[len_index]); i++){
        if(map[i].key == NULL)
            return kFIRST;
        if(map[i].key[keyword.len] == toupper(keyword.position[keyword.len]) &&
           strncasecmp((const char *)keyword.position, map[i].key, keyword.len))
            return map[i].hkey;
    }

    return kFIRST;
}
