#include "org_nodes.h"
#include "scanners.h"
#include <yajl/yajl_gen.h>

#define JS_STR(G, STR) do {                                             \
        const char *_s = (STR);                                         \
        yajl_gen_string(G, (const unsigned char *)_s, strlen(_s));      \
        } while (0)

#define JS_string(G, STR) yajl_gen_string(G, STR.position, STR.len);

void render_node_json_recusive(struct node *node, yajl_gen *g){

    int child_done = FALSE ;

    yajl_gen_map_open(*g);

    JS_STR(*g, "type");
    yajl_gen_string(*g, (const unsigned char *) org_node_t_str[node->type], strlen(org_node_t_str[node->type]));


    switch(node->type){
    case onBold:
    case onItalic:
    case onVerbatim:
    case onUnderline:
    case onStrike_Through:{
        //PUTS(",");
        JS_STR(*g, "plist");
        yajl_gen_map_open(*g);
        JS_STR(*g, "type");
        /* long int t =  node->plist.emph->type; */
        /* yajl_gen_integer(*g, t) ; */
        yajl_gen_string(*g, (const unsigned char *) &node->plist.emph->type, 1) ;
        yajl_gen_map_close(*g);

        JS_STR(*g, "children");
        yajl_gen_array_open(*g);
        render_node_json_recusive(node->first_child, g);
        yajl_gen_array_close(*g);
        child_done = TRUE;

        break;
    }
    case onSubscript: {
        //PUTC('_');
        break;
    }
    case onSuperscript:{
        //PUTC('^');
        break;
    }
    case onSpecial_Block: {
        //PUTS("#+begin:");
        if(node->text.position && node->text.len >0){
            JS_STR(*g, "test");
            JS_string(*g, node->text);
            //yajl_gen_string(*g, node->text.position, node->text.len);
                //PUTSJSON_kcv("text", );
        }
        //PUTS("#+end:");
        break;
    }
    case onLatex_Fragment:{
        JS_STR(*g, "text");
        JS_string(*g, node->text);
        break;
    }
    case onLatex_Environment:{
        JS_STR(*g, "text");
        JS_string(*g, node->text);
        break;
    }
    case onDrawer:{
        JS_STR(*g, "text");
        JS_string(*g, node->text);
        break;
    }
    case onExport_Snippet:{
        JS_STR(*g, "plist");
        yajl_gen_map_open(*g);
        JS_STR(*g, "back_end");
        JS_string(*g, node->plist.exp_snip->back_end);
        JS_STR(*g, "value");
        JS_string(*g, node->plist.exp_snip->value);
        yajl_gen_map_close(*g);
        break;
    }
    case onSrc_Block:
    case onVerse_Block:
    case onCenter_Block:
    case onExample_Block:
    case onComment_Block:
    case onExport_Block:
    case onQuote_Block:{
        //PUTS("#+begin_"); PUTS(begin_blocks[node_t_to_obt(node->type)].name_lower);

        if(node->type == onSrc_Block || node->type == onExport_Block) {
            JS_STR(*g, "plist");
            yajl_gen_map_open(*g);
            JS_STR(*g, "lang");
            JS_string(*g, node->plist.se_blocks->lang);
            yajl_gen_map_close(*g);
        }
        if(node->text.position && node->text.len >0){
            JS_STR(*g, "text");
            JS_string(*g, node->text);
        }
        break;
    }
    case onItem:{
        struct corg_list_item *li = node->plist.li;
        JS_STR(*g, "plist");
        yajl_gen_map_open(*g);
        JS_STR(*g, "preblank");
        yajl_gen_integer(*g, li->pre_blank);


        JS_STR(*g, "bullet");
        JS_string(*g, li->bullet);

        if(li->counter != -1){
            JS_STR(*g, "counter");
            yajl_gen_integer(*g, li->counter);
        }
        if(li->checkbox != CORG_LIST_CHECKBOX_NIL){
            JS_STR(*g, "checkbox");
            switch(li->checkbox){
            case CORG_LIST_CHECKBOX_OFF: {JS_STR(*g, " ");break; }
            case CORG_LIST_CHECKBOX_ON: {JS_STR(*g, "X");break; }
            case CORG_LIST_CHECKBOX_TRANS: {JS_STR(*g, "-");break; }
            default:
                assert(0 && "unreachable");
            }
        }
        if(li->tag.len !=0){
            JS_STR(*g, "tag");
            JS_string(*g, li->tag);
        }
        yajl_gen_map_close(*g);
        break;
    }
    case onKeyword: {
        struct corg_keyword *kw = node->plist.keyword;
        JS_STR(*g, "plist");
        yajl_gen_map_open(*g);
        JS_STR(*g, "key");
        JS_string(*g, kw->key);
        JS_STR(*g, "value");
        JS_string(*g, kw->value);
        JS_STR(*g, "keyn");
        yajl_gen_integer(*g, kw->keyt);
        yajl_gen_map_close(*g);

        break;
    }
    case onProperty_Drawer: {
        break;
    }
    case onNode_Property: {
        JS_STR(*g, "plist");
        yajl_gen_map_open(*g);
        JS_STR(*g, "key");
        JS_string(*g, node->plist.kv->key);
        JS_STR(*g, "value");
        JS_string(*g, node->plist.kv->value);
        JS_STR(*g, "keyn");
        yajl_gen_integer(*g, node->plist.kv->keyt);
        yajl_gen_map_close(*g);

        break;
    }
    case onMacro: {
        JS_STR(*g, "plist");
        yajl_gen_map_open(*g);
        JS_STR(*g, "key");
        JS_string(*g, node->plist.kv->key);
        if(node->plist.kv->value.len != 0){
            JS_STR(*g, "value");
            JS_string(*g, node->plist.kv->value);
        }
        yajl_gen_map_close(*g);
        break;
    }

    case onLink: {
        struct corg_link *link = node->plist.link;
        const char *beg[]={"[[", "<", ""};
        JS_STR(*g, "plist");
        yajl_gen_map_open(*g);
        JS_STR(*g, "format");
        yajl_gen_string(*g, (const unsigned char *)beg[link->format], strlen(beg[link->format])) ;

        JS_STR(*g, "type");
        JS_string(*g, link->type);

        JS_STR(*g, "path");
        JS_string(*g, link->path);
        if(link->format == olfBracket && node->first_child != 0){
            JS_STR(*g, "children");
            yajl_gen_array_open(*g);
            render_node_json_recusive(node->first_child, g);
            yajl_gen_array_close(*g);

        }

        //const char *end[]={"]]", ">", ""};
        //PUTS(end[link->format]);
        yajl_gen_map_close(*g);
        break;
    }
    case onLeaf: {
        if(node->text.position && node->text.len >0){
            JS_STR(*g, "text");
            JS_string(*g, node->text);
        }
        break;
    }
    case onHeadline:{
        struct corg_headline hd = *node->plist.headline;
        //for(int i=0; i<hd.level; i++) PUTC('*');

        JS_STR(*g, "plist");
        yajl_gen_map_open(*g);
        JS_STR(*g, "level");
        yajl_gen_integer(*g, hd.level) ;


        if(hd.todo_keyword.len > 0)  {
            JS_STR(*g, "todo_keyword");
            JS_string(*g, hd.todo_keyword) ;
        }
        if(hd.priority)  {
            JS_STR(*g, "priority");

            yajl_gen_string(*g, (const unsigned char *) &hd.priority, 1) ;
        }

        JS_STR(*g, "raw_value");
        JS_string(*g, hd.raw_value) ;


        if(hd.tags.len > 0)  {
            JS_STR(*g, "tags");
            JS_string(*g, hd.tags) ;

        }
        if(hd.planning[HEADLINE_PLANNING_DEADLINE].year_start + hd.planning[HEADLINE_PLANNING_SCHEDULED].year_start + hd.planning[HEADLINE_PLANNING_CLOSED].year_start != 0) {
            JS_STR(*g, "planning");
            yajl_gen_map_open(*g);

            if(hd.planning[HEADLINE_PLANNING_DEADLINE].year_start != 0){
                struct timestamp *ts = &hd.planning[HEADLINE_PLANNING_DEADLINE];
                JS_STR(*g, "deadline");
                JS_string(*g, ts->raw_value);
            }
            if(hd.planning[HEADLINE_PLANNING_SCHEDULED].year_start != 0){
                struct timestamp *ts = &hd.planning[HEADLINE_PLANNING_SCHEDULED];
                JS_STR(*g, "scheduled");
                JS_string(*g, ts->raw_value);
            }
            if(hd.planning[HEADLINE_PLANNING_CLOSED].year_start != 0){
                struct timestamp *ts = &hd.planning[HEADLINE_PLANNING_CLOSED];
                JS_STR(*g, "closed");
                JS_string(*g, ts->raw_value);
            }
            yajl_gen_map_close(*g);
        }
        yajl_gen_map_close(*g);
        break;
    }
    case onTimestamp:{
        struct timestamp *ts = &node->plist.timestamp->ts;
        JS_STR(*g, "text");
        JS_string(*g, ts->raw_value) ;

        break;
    }
    case onFixed_width:{
        //PUTC(':');
        JS_STR(*g, "text");
        JS_string(*g, node->text) ;

        break;
    }
    case onComment:{
        //PUTC(':');
        JS_STR(*g, "text");
        JS_string(*g, node->text) ;
        break;
    }
    case onHorizontal_Rule:{
        //PUTS("-----");
        break;
    }
    case onStatistics_Cookie: {
        JS_STR(*g, "text");
        JS_string(*g, node->text) ;
        break;
    }
    case onFootnote_Definition:
    case onFootnote_Reference:{
        struct corg_footnote *ftnt = node->plist.fn;
        JS_STR(*g, "plist");
        yajl_gen_map_open(*g);
        if(ftnt->label.position){
            JS_STR(*g, "label");
            JS_string(*g, ftnt->label);
        }
        if(ftnt->value.len!=0){
            JS_STR(*g, "value");
            JS_string(*g, ftnt->value);
        }
        yajl_gen_map_close(*g);
        break;
    }
    case onEntity: {
        JS_STR(*g, "text");
        JS_string(*g, node->text) ;
        break;
    }
    case onLine_Break: {
        //PUTC('\n');
        break;
    }

    default:
        break;
    }

    if((child_done==FALSE) && node->first_child){
        JS_STR(*g, "children");
        yajl_gen_array_open(*g);
        render_node_json_recusive(node->first_child, g);
        yajl_gen_array_close(*g);
    }

    yajl_gen_map_close(*g);
    if(node->next && (node->prev==NULL)){
        struct node *iter = node->next;
        while(iter != NULL){
            render_node_json_recusive(iter, g);
            iter=iter->next;
        }
    }
}


#include <string.h>
char *render_json(struct corg_ctx *ctx){

    yajl_gen g;

    const unsigned char *buf;
    size_t len;
    g = yajl_gen_alloc(NULL);
    yajl_gen_array_open(g);
    render_node_json_recusive(ctx->document, &g);
    yajl_gen_array_close(g);
    yajl_gen_get_buf(g, &buf, &len);
    char *res = malloc(len+1);
    strncpy(res,(const char *) buf, len);
    res[len]=0;
    yajl_gen_free(g);
    return res;
}

void print_json(struct corg_ctx *ctx){
    char *out = render_json(ctx);
    printf("%s", out);
    free(out);
}
