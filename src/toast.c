#include <signal.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include "org_nodes.h"
#include "scanners.h"

#define alloc_struct(p)                         \
            do {                                \
                typeof(*p) aszero_ ## p = {0};  \
                p = malloc(sizeof *p);          \
                *p = aszero_ ## p;              \
            } while (0)

struct corg_headline ch0 = {0};
struct timestamp tsc0 = {0};

int ast_header(void *data, struct title_pointers ti){
    assert((int)(TITI2 - TITI1) >=0);

    struct node *nheadline = new_node(onHeadline, data);
    //    *headline->plist.headline = ch0;
    struct corg_headline *hd = nheadline->plist.headline;
    hd->priority = 0;
    hd->level = (int)(TIST2 - TIST1);
    //printf("start   :     %.*s\n", (int)(TIST2 - TIST1), TIST1);
    if(TIKW1)  {
        hd->todo_keyword.position = TIKW1;
        hd->todo_keyword.len = (int)(TIKW2 - TIKW1);
    }
    if(TIPR1)  {
        hd->priority = TIPR1[0];
    }
    hd->raw_value.position = TITI1;
    hd->raw_value.len = (int)(TITI2 - TITI1);

    if(TITG1)  {
        hd->tags.position = TITG1;
        hd->tags.len = (int)(TITG2 - TITG1);
    }
    struct corg_ctx *ctx = (struct corg_ctx * )data;
    hd->line = ctx->line;
    insert_last(ctx->document, nheadline);
    return 0;
}


int toint(const unsigned char *str, int num, int base){
    int res =0;
    for (int i=0; i<num;i++){
        res += (int)(str[i] - '0') * pow(base, (num-i-1));
    }
    return  res;
}

const unsigned char* parse_rw_vals(const unsigned char *val, const unsigned char *limit, int *out_value, char *out_unit){
    const unsigned char *num_end=val;
    while(num_end < limit && *num_end >= '0' && *num_end <= '9'){
        num_end++;
    }
    *out_value = toint(val, num_end-val, 10);
    *out_unit = *num_end;
    num_end++;
    if(*num_end==' ') num_end++;
    return num_end;
}

#include <stddef.h>
void str_to_timestamp(struct ts_pointers ts, struct timestamp *out){
    // <2018-07-06 Fri 09:02>
    //  0123456789

    *out = tsc0;
    out->raw_value.position = TTYEAR1-1;
    out->repeater_type = TS_RPT_NONE;
    out->warning_type = TS_WRN_NONE;
    out->active = (*(TTYEAR1-1) == '<') ? TRUE : FALSE ;
    out->year_start = toint(TTYEAR1, 4, 10);
    out->month_start = toint(TTYEAR1+5, 2, 10);
    out->day_start = toint(TTYEAR1+8, 2, 10);
    const unsigned char *end=NULL;

    if(TTMIN2 != NULL){
        out->hour_start = toint(TTHOUR1, TTHOUR2-TTHOUR1, 10);
        out->minute_start = toint(TTMIN2-2, 2, 10);
        end = TTMIN2;
        if(TTTRM2 != NULL){
            out->hour_end = toint(TTTRH1, TTTRH2-TTHOUR1, 10);
            out->minute_end = toint(TTTRM2-2, 2, 10);
            end = TTTRM2;
        }
    }
    else{
        end = TTREP1;
        //out->raw_value.len = (int)(TTVE - TTYEAR1+2);
    }
    if(TTVE-end != 0){
        end++; // " "
        ptrdiff_t rpt_len = TTVE-end;
        //++9m
        while(end < TTVE){
            switch(*end){
            case '.':
            case '+':{
                out->repeater_type = TS_RPT_CUMULATE;
                const unsigned char *val = end+1;
                if(*(end) == '.'){
                    out->repeater_type = TS_RPT_RESTART;
                    val++;
                }else if (*(end+1) == '+'){
                    out->repeater_type = TS_RPT_CATCHUP;
                    val++;
                }
                end = parse_rw_vals(val, TTVE,
                                    &out->repeater_value,
                                    &out->repeater_unit);
                break ;
            }
            case '-':{
                out->warning_type = TS_WRN_ALL;
                const unsigned char *val = end+1;
                if (*(end+1) == '-'){
                    out->warning_type = TS_WRN_FIRST;
                    val++;
                }
                end = parse_rw_vals(val, TTVE,
                                    &out->warning_value,
                                    &out->warning_unit);
                break ;
            }
            default:{
                assert(0 && "Timestamp parsing: wrong repeater / warning logic");
            }
            }
        }


    }

    out->raw_value.len = (int)(end - TTYEAR1+2);

    assert(((*(out->raw_value.position + out->raw_value.len -1) == '>') ||
            (*(out->raw_value.position + out->raw_value.len -1) == ']')));
}

int ast_planning(void *header_data, struct ts_pointers ts){

    struct corg_ctx *ctx = (struct corg_ctx * )header_data;
    ctx->line++;
    struct corg_headline *hd = ctx->document->last_child->plist.headline;


    struct timestamp tsout;
    str_to_timestamp(ts, &tsout);

    int index = 0;
    if(TTKW1){
        switch(TTKW1[1]){
        case 'C':
            index = HEADLINE_PLANNING_SCHEDULED;
            break;
        case 'E':
            index = HEADLINE_PLANNING_DEADLINE;
            break;
        case 'L':
            index = HEADLINE_PLANNING_CLOSED;
            break;
        default:
            index = 0;
            break;
        }
    }
    hd->planning[index] = tsout;
    return 0;
}


int ast_prop_drawer(void *header_data, const unsigned char *propd_beg, const unsigned char *propd_end){
    struct corg_ctx *ctx = (struct corg_ctx * )header_data;
    ctx->line+=2; // #+PROPER..
    const unsigned char *cursor = propd_beg;
    struct node *npd = new_node(onProperty_Drawer, ctx);
    while (cursor < propd_end){
        struct markers ma;
        int eprop = which_node_prop(&cursor, &ma);
        if(eprop==-1){
            ctx->cursor = cursor;
            ctx->cursor--;
            break;
        }
        ctx->line++;

        struct node *nnodep = new_node(onNode_Property, ctx);
        nnodep->plist.kv->keyt = eprop;
        SET_STRING(nnodep->plist.kv->key, MA_KEYB, (int)(MA_KEYE-MA_KEYB));
        if(MA_VALUEB){
            SET_STRING(nnodep->plist.kv->value, MA_VALUEB, (int)(MA_VALUEE-MA_VALUEB));
        }
        insert_last(npd, nnodep);
    }
    insert_last_headline(ctx, npd);
    return 0;
}

int ast_options(void *header_data, const unsigned char *YYCURSOR, const unsigned char *optkw1, const unsigned char *optkw2){
    UNUSED(header_data);
    UNUSED(optkw1);
    UNUSED(optkw2);
    UNUSED(YYCURSOR);
    //printf("OPTIONS -- %.*s %.*s\n", (int)(optkw2 - optkw1), optkw1, (int)(YYCURSOR - optkw2), optkw2);
   return 0;
}


#include <ctype.h>

int block_logic(const unsigned char *blktype, unsigned char *blocktype, int end){
    switch(*blktype){
            case '_': {
                if((end == TRUE) && (toupper(blktype[3]) == *blocktype)){
                    return TRUE;
                };
                if( (end != TRUE) && (*blocktype == 0)) {
                    *blocktype = toupper(blktype[3]);
                    return TRUE;
                }
                break;
            }
            case ':':
            case ' ': {
                if(end && (blktype[0] == toupper(*blocktype))) {
                    //(*blocktype)=0;
                    return TRUE;
                }
                if( (end != TRUE) && (*blocktype == 0)) {
                    *blocktype = toupper(blktype[0]);
                    return TRUE;
                }
                break;
            }
            default:
                assert("unknown block type");
            }
    return FALSE;
}

void new_ctx(const char *input, struct corg_ctx *ctx){
    ctx->ascii_table[(int) '*'] = onBold;
    ctx->ascii_table[(int) '~'] = onVerbatim;
    ctx->ascii_table[(int) '+'] = onStrike_Through;
    ctx->ascii_table[(int) '='] = onVerbatim;
    ctx->ascii_table[(int) '_'] = onUnderline;
    ctx->ascii_table[(int) '/'] = onItalic;
    ctx->ascii_table[(int) '['] = onLink;
    ctx->ascii_table[(int) '{'] = onMacro;

    ctx->ascii_table[(int) 'C'] = onSrc_Block;
    ctx->ascii_table[(int) 'R'] = onVerse_Block;
    ctx->ascii_table[(int) 'N'] = onCenter_Block;
    ctx->ascii_table[(int) 'A'] = onExample_Block;
    ctx->ascii_table[(int) 'M'] = onComment_Block;
    ctx->ascii_table[(int) 'O'] = onQuote_Block;
    ctx->ascii_table[(int) 'P'] = onExport_Block;
    ctx->ascii_table[(int) ':'] = onSpecial_Block;

    ctx->copy_strings = FALSE;
    ctx->use_node_uuids = FALSE;

    if(ctx->use_node_uuids){
        ctx->node_uids = malloc(sizeof(long long) * (onLAST - onFIRST));
        memset(ctx->node_uids, 0,
               sizeof(long long) * (onLeaf - onDocument + 1));
    } else
        ctx->node_uids = NULL;

    omt_arena *arena = &ctx->node_pool;
    int use_arena = 0;
    if(use_arena){
        arena->head = malloc(sizeof(omt_arena_chunk));
        memset(arena->head, 0, sizeof(omt_arena_chunk));
        arena->allocation_ptr = arena->head->data;
    } else {
        arena->head = NULL;
        arena->allocation_ptr = NULL;
    }

    for(size_t i=0; i < sizeof(ctx->last_by_type) / sizeof(ctx->last_by_type[0]); i++)
        ctx->last_by_type[i]=NULL;


    ctx->document = new_node(onDocument, ctx);
    ctx->input_string = (const unsigned char*)input;
    ctx->input_string_len = strlen(input);
    ctx->cursor = (const unsigned char*) input;
    ctx->line = 0;

}

void parse_row(struct corg_ctx *ctx){
    int wsp=0;
    int wsp_before=0;
    int trim_wsp=TRUE;
    struct node *nrow = new_node(onTable_Row, ctx);
    const unsigned char *cellb=0;
    assert(*ctx->cursor == '|');
    struct node *table = NULL;
    if(ctx->last_by_type[onTable] == NULL || nrow->prev_by_type == NULL || (nrow->prev_by_type && nrow->prev_by_type->line != ctx->line-1)){
        table = new_node(onTable, ctx);
        insert_last_headline(ctx, table);
    } else {
        table = ctx->last_by_type[onTable];

    }
    insert_last(table, nrow);
    if(*(ctx->cursor+1) && (*(ctx->cursor+1)=='-')){
        return;
    }
    while(*ctx->cursor && *ctx->cursor != '\n'){
        switch(*ctx->cursor){
        case ' ':
        case '\t':
            wsp++;
            break;
        case '|':
            if(cellb != 0) {
                struct node *ncell = new_node(onTable_Cell, ctx);
                int len = (trim_wsp ? ctx->cursor-wsp:ctx->cursor)
                    - (trim_wsp ? cellb+wsp_before:cellb)-1;
                assert(len>=0);
                struct node *leaf = new_leaf_node_with_text(cellb+1, ctx->cursor-cellb-1, ctx);
                insert_last(ncell, leaf);
                insert_last(nrow, ncell);
            }
            cellb = ctx->cursor;
            wsp=wsp_before=0;
            break;
        default:
            if(wsp_before==0) wsp_before=wsp;
            wsp=0;
        }
        ctx->cursor++;
    }


}

enum corg_greater_elements {
    corg_ge_begin_block,
    corg_ge_ltx_env,
    corg_ge_drawer,
    corg_ge_footnode_def,
    corg_ge_headline
};

struct pending_ge_info {
    const unsigned char *start_cursor;
    int start_status;
    int start_line;
    const unsigned char *marker_preblank;
    ptrdiff_t start_preblank;
    union{
        struct {
            const unsigned char *data_marker;
            unsigned char id;
            struct string blk_lang;
        } begin_block;
        struct{
            struct string drawer_name;
        } drawer;
        struct {
            struct string ltx_env;
        } ltx_env;
    }u;
};

void set_pending_ge(struct corg_ctx *ctx, enum corg_bol_t status, struct pending_ge_info *pge, struct corg_bol_ret *ret){
    pge->start_status = status;
    pge->start_line = ctx->line;
    pge->marker_preblank = ctx->cursor;
    pge->start_preblank = ret->pre_blank;
}

#define NO_PENDING_BLOCK (-1)
static struct string str0 = {0};
static struct pending_ge_info pge0 = {0};

void init_pending_ge(struct pending_ge_info *pge){
    *pge = pge0;
    pge->start_status = NO_PENDING_BLOCK;
}
void reset_pending_ge(struct corg_ctx *ctx, struct pending_ge_info *pge){
    ctx->line = pge->start_line;
    ctx->cursor = pge->marker_preblank;
    init_pending_ge(pge);
}

#include <strings.h>

void parse_nonge(struct corg_ctx *ctx, enum corg_bol_t status, struct corg_bol_ret ret){
    const unsigned char * nlcursor = ctx->cursor;
    switch(status){
    case CORG_BOL_TABLE:
        ctx->cursor += ret.pre_blank;
        parse_row(ctx);
        break;
    case CORG_BOL_LIST_ITEM: {
        struct node *nli = new_node(onItem, ctx);
        nli->plist.li->pre_blank = ret.pre_blank;
        const unsigned char *li_start = ctx->cursor+ret.pre_blank;
        SET_STRING(nli->plist.li->bullet, li_start, (ptrdiff_t)(ret.optkw2-li_start +1));

        li_start+=(ptrdiff_t)(ret.optkw2-li_start)+1;
        parse_list_item(&li_start, nli);
        ctx->cursor = li_start;
        insert_last_headline(ctx, nli);
        lex_line(ctx);
        break;
    }
    case CORG_BOL_FOOTNOTE_DEF:{
        struct node *nfn_def = new_node(onFootnote_Definition, ctx);
        insert_last_headline(ctx, nfn_def);
        nfn_def->plist.fn->type = FOOTNOTE_DEFINITION;
        SET_STRING(nfn_def->plist.fn->label, ret.optkw1, (ptrdiff_t)(ret.optkw2-ret.optkw1));
        ctx->cursor += (ret.optkw2-ctx->cursor)+ 1;
        lex_line(ctx);
        break;
    }
    case CORG_BOL_KEYWORD:
    case CORG_BOL_KEYWORD_CUSTOM:
    case CORG_BOL_KEYWORD_OX:{
        struct node *keywordn = new_node(onKeyword, ctx);
        struct corg_keyword *kw = keywordn->plist.keyword;
        SET_STRING(kw->key, ctx->cursor+ret.pre_blank+2, (ptrdiff_t)(ret.optkw1- ctx->cursor)-ret.pre_blank-2);
        SET_STRING(kw->value, ret.optkw1, (ptrdiff_t)(ret.optkw2-ret.optkw1));
        kw->keyt = kFIRST;
        if(status == CORG_BOL_KEYWORD)
            kw->keyt = wkey(kw->key);
        else
            kw->keyt = recon_keyword(kw->key);
        insert_last_headline(ctx, keywordn);
        if(kw->key.len==strlen("CORGM_LINE:") && 0==strncasecmp((const char *)kw->key.position, "CORGM_LINE:", strlen("CORGM_LINE:"))){
            assert( ctx->line == atoi((const char *)kw->value.position));
        }
        //printf("kw:       %ld\n", ctx->line);
        break;
    }
    case CORG_BOL_FIXED_WIDTH: {
        struct node *fwn = new_node(onFixed_width, ctx);
        nlcursor = ctx->cursor + ret.pre_blank +1;
        while(*ctx->cursor && *ctx->cursor != '\n') ctx->cursor++;
        SET_STRING(fwn->text, nlcursor, ctx->cursor-nlcursor);
        insert_last_headline(ctx, fwn); ctx->cursor--;
        break;
    }
    case CORG_BOL_COMMENT: {
        struct node *ncmt = new_node(onComment, ctx);
        nlcursor = ctx->cursor + ret.pre_blank +1;
        while(*ctx->cursor && *ctx->cursor != '\n') ctx->cursor++;
        SET_STRING(ncmt->text, nlcursor, ctx->cursor-nlcursor);
        insert_last_headline(ctx, ncmt); ctx->cursor--;
        break;
    }
    case CORG_BOL_HRULE: {
        struct node *hr = new_node(onHorizontal_Rule, ctx);
        insert_last_headline(ctx, hr);
        break;
    }
    case CORG_BOL_NEWLINE:
        //ctx->cursor++;ctx->line++;
        //printf("newline:  %ld\n", ctx->line);
        break;
    case CORG_BOL_EOF:
        assert(1);
        break;
    case CORG_BOL_OTHER:
        lex_line(ctx);
        break;
    default:
        assert(0 && "corg_parse_to_ast should handle this type");
    }
}

struct corg_ctx *corg_parse_to_ast(char* buf){
    struct corg_bol_ret ret;
    unsigned char *buffer = (unsigned char*) buf;
    struct corg_ctx *ctx=malloc(sizeof(struct corg_ctx));
    new_ctx((char *)buffer, ctx);
    struct node *document = ctx->document;
    ctx->line = 0;
    const unsigned char * nlcursor = ctx->cursor;
    enum corg_bol_t status = whatbol(nlcursor, &ret);
    struct pending_ge_info pblk;
    init_pending_ge(&pblk);

    int goto_header=1;
    while( *ctx->cursor && (status) != CORG_BOL_EOF ){
        switch(status){
        case CORG_BOL_HEADLINE:
            //printf("headline: %ld\n", ctx->line);
            goto_header = 1;
            switch(pblk.start_status){
            case CORG_BOL_BLOCK_BEGIN: {
                log_warn("Wrong begin block backtracking line:%d", pblk.start_line);
                reset_pending_ge(ctx, &pblk);
                lex_line(ctx);goto_header=0;
                break;
            }
            case CORG_BOL_LATEXENV_BEG:{
                log_warn("Wrong latex environment -> backtracking line:%d", pblk.start_line);
                reset_pending_ge(ctx, &pblk);
                lex_line(ctx);goto_header=0;
                break;
            }
            case CORG_BOL_DRAWER_BEG: {
                log_warn("Wrong drawer begin -> backtracking line:%d", pblk.start_line);
                reset_pending_ge(ctx, &pblk);
                lex_line(ctx);goto_header=0;
                break;
            }
            case NO_PENDING_BLOCK: {
                break;
            }
            default:
                assert("Not Implemented");
            }
            if(goto_header)
                parse_header(ctx, ctx, &ast_header, &ast_planning, &ast_prop_drawer);
            break;
        case CORG_BOL_BLOCK_BEGIN:
            if(pblk.start_status==NO_PENDING_BLOCK && block_logic(ret.blktype, &pblk.u.begin_block.id, FALSE)==TRUE) {
                pblk.marker_preblank=ctx->cursor;
                if(ret.optkw1){
                    SET_STRING(pblk.u.begin_block.blk_lang, ret.optkw1, (ptrdiff_t)(ret.optkw2 - ret.optkw1));
                }
                if(ctx->ascii_table[pblk.u.begin_block.id] != onSpecial_Block)
                    pblk.u.begin_block.data_marker=ret.blktype + begin_blocks[node_t_to_obt(ctx->ascii_table[pblk.u.begin_block.id])].len+1;
                else
                    pblk.u.begin_block.data_marker=ret.blktype;
                set_pending_ge(ctx, status,&pblk, &ret);
            }
            break;
        case CORG_BOL_BLOCK_END:{
            if (pblk.start_status==CORG_BOL_BLOCK_BEGIN && block_logic(ret.blktype, &pblk.u.begin_block.id, TRUE) == TRUE){
                assert(pblk.marker_preblank!=0);
                struct node *nblock = new_node(ctx->ascii_table[pblk.u.begin_block.id], ctx);
                pblk.start_status = NO_PENDING_BLOCK;
                int skipped_for_lang=0;
                if(pblk.u.begin_block.blk_lang.len != 0) {
                    nblock->plist.se_blocks->lang.position = pblk.u.begin_block.blk_lang.position;
                    nblock->plist.se_blocks->lang.len = pblk.u.begin_block.blk_lang.len;
                    skipped_for_lang = pblk.u.begin_block.blk_lang.position + pblk.u.begin_block.blk_lang.len - pblk.u.begin_block.data_marker;
                }
                SET_STRING(nblock->text, pblk.u.begin_block.data_marker + skipped_for_lang, ctx->cursor-pblk.u.begin_block.data_marker - skipped_for_lang);

                insert_last(document, nblock);
                init_pending_ge(&pblk);
            } else if(pblk.start_status == NO_PENDING_BLOCK){
                lex_line(ctx);
            }
            break;
        }
        case CORG_BOL_DRAWER_BEG:{
            if(pblk.start_status==NO_PENDING_BLOCK){
                set_pending_ge(ctx, status, &pblk, &ret);
                SET_STRING(pblk.u.drawer.drawer_name, ret.optkw1-1, (ptrdiff_t)(ret.optkw2 - ret.optkw1+1));
            }
            break;
        }
        case CORG_BOL_DRAWER_END: {
            if(pblk.start_status == CORG_BOL_DRAWER_BEG){
                struct node *ndrawer = new_node(onDrawer, ctx);
                SET_STRING(ndrawer->text, pblk.u.drawer.drawer_name.position, (ptrdiff_t)(ret.optkw2 - pblk.u.drawer.drawer_name.position)+1);
                insert_last(document, ndrawer);
                init_pending_ge(&pblk);
            } else if(pblk.start_status == NO_PENDING_BLOCK){
                lex_line(ctx);
            }
            break;
        }
        case CORG_BOL_LATEXENV_BEG:{
            if(pblk.start_status==NO_PENDING_BLOCK){
                set_pending_ge(ctx, status, &pblk, &ret);
                SET_STRING(pblk.u.ltx_env.ltx_env, ret.optkw1, (ptrdiff_t)(ret.optkw2 - ret.optkw1));
            }
            break;
        }
        case CORG_BOL_LATEXENV_END: {
            if(pblk.start_status==CORG_BOL_LATEXENV_BEG && pblk.u.ltx_env.ltx_env.position && (pblk.u.ltx_env.ltx_env.len == (ptrdiff_t)(ret.optkw2 - ret.optkw1))){
                int i=0;
                for(i=0; i<pblk.u.ltx_env.ltx_env.len && pblk.u.ltx_env.ltx_env.position[i] == ret.optkw1[i]; i++) {;}
                if(i==pblk.u.ltx_env.ltx_env.len){
                    struct node *nltxenv = new_node(onLatex_Environment, ctx);
                    SET_STRING(nltxenv->plist.latex_env->environment, ret.optkw1, (ptrdiff_t)(ret.optkw2 - ret.optkw1));
                    SET_STRING(nltxenv->text, pblk.u.ltx_env.ltx_env.position - (pblk.start_preblank) - strlen("\\begin{"), (ptrdiff_t)(ret.optkw2+1 - pblk.u.ltx_env.ltx_env.position) + (pblk.start_preblank) + strlen("\\begin{"));
                    insert_last(document, nltxenv);
                    init_pending_ge(&pblk);
                }
            }else if(pblk.start_status == NO_PENDING_BLOCK){
                lex_line(ctx);
            }
            break;
        }
        default:{
            if(pblk.start_status == NO_PENDING_BLOCK)
                parse_nonge(ctx, status, ret);
        }
        }
        assert(ctx->cursor >= (ctx->input_string));
        assert(ctx->cursor <= (ctx->input_string + ctx->input_string_len ));
        while(*ctx->cursor && *ctx->cursor != '\n') ctx->cursor++;
        if(*ctx->cursor) {
            ctx->line++;
            ctx->cursor++;
            if(pblk.start_status==NO_PENDING_BLOCK) {
                struct node *nl = new_node(onLine_Break, ctx);
                struct node *into  = ctx->document;
                struct node *lc = ctx->document->last_child;
                if(lc && lc->type == onParagraph && (lc->last_child && lc->last_child->type != onLine_Break))
                    into = lc;
                insert_last(into, nl);
            }
        }
        nlcursor = ctx->cursor;
        status = whatbol(nlcursor, &ret);
    };
    corg_node_check(document);
    return ctx;
}
