#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "header.h"
#include "org_nodes.h"
#include "scanners.h"


void mtv_push(struct mtagsv_t *v, struct mtags_t x) {
    if (v->n == v->m) {
        v->m = v->m? v->m<<1 : 2;
        v->a = realloc(v->a, sizeof(struct mtags_t) * v->m);
    }
    v->a[v->n++] = (x);
}
#define YYMTAGP(t) do{ struct mtags_t l = {t, YYCURSOR}; t = (int) mtv_size(array); mtv_push(&array, l);}while(0)
#define YYMTAGN(t) do{ struct mtags_t l = {t, NULL}; t = (int) mtv_size(array); mtv_push(&array, l);}while(0)

/*!max:re2c*/

/*!re2c
        re2c:define:YYCTYPE = "unsigned char";
        re2c:yyfill:enable = 0;
        re2c:flags:debug-output = 0;

        end  = "\x00";
        eol  = "\n";
        sep  = [:];
        wsp  = [ \t]*;
        keyword = @TIKW1 ("TODO" | "DONE") @TIKW2 " ";
        char = [^\x00\n]; //[^] \ (end | eol);
        title = char+;
        tag = [a-zA-Z0-9_@#%]+ ;
        alnum = [a-zA-Z0-9];
        //tags = wsp @TITG1 sep (tag sep)+ @TITG2 wsp (end | eol);
        priority = "[#" @TIPR1 ([A-Z0-9]) @TIPR2 "]"  " ";
        start = [*];
        digit = [0-9];
        dsep = [-];

//emphasis
        emph_pre = [\-\x20\t"{(];
        emph_post = [{\-\t\x20.,:!?;")}];
        emph_border=[^\x00\x20\t\n\r];
        // bold      = emph_pre "*" @MA_EMPHB emph_border ((char\[*])* emph_border)? @MA_EMPHE "*" emph_post;
        // italic    = emph_pre "/" @MA_EMPHB emph_border ((char\[/])* emph_border)? @MA_EMPHE "/" emph_post;
        // underline = emph_pre "_" @MA_EMPHB emph_border ((char\[_])* emph_border)? @MA_EMPHE "_" emph_post;
        // verbatim  = emph_pre [~=] @MA_EMPHB emph_border ((char\[~=])* emph_border)? @MA_EMPHE [~=] emph_post;
        // strike    = emph_pre "+" @MA_EMPHB emph_border ((char\[+])* emph_border)? @MA_EMPHE "+" emph_post;

        macro = "{{{" @MA_KEYB [a-zA-Z][-a-zA-Z0-9_]* @MA_KEYE ( wsp @MA_VALUEB "(" ( "}}" [^}\x00\n\r] | "}" [^}\x00\n\r] | [^}\x00\n\r])* "}"{0,2} ")" ){0,1} @MA_VALUEE "}}}";

        subsuperscript = [^\x00\t \n\t] [_^] @MA_VALUEB( "(" [^}{\x00\n\r]*  ")" | "{" [^}{\x00\n\r]*  "}" | ( "*" | [+-]? (alnum | [.,\\])* alnum)) @MA_VALUEE;

//links
        target  = "<<" @MA_VALUEB ([^<>\x00\t \n] | [^<>\x00\t \n] [^<>\x00\t\n]* [^<>\x00\t \n]) @MA_VALUEE ">>";
        radio_target = "<" target ">" ;


        links_type = "rmail" | "mhe" | "irc" | "info" | "gnus" | "docview" | "bibtex" | "bbdb" | "w3m" | "deft" | "bookmark-other-win" | "bookmark" | "pdf-extern" | "file+sys" | "file+emacs" | "doi" | "elisp" | "file" | "ftp" | "help" | "http" | "https" | "mailto" | "news" | "shell" ; // (org-link-types)

        customid_link = "[[#" @MLKPATHB [a-z-A-Z0-9-]+ @MLKPATHE "]]";
        //[a-zA-Z0-9+]+  <-> links_type ?
        bracket_link_ext = "[[" @MLKTYPEB links_type @MLKTYPEE ":" @MLKPATHB [^[\]\x00\n]+ @MLKPATHE "]" "[" @MLKDESCRIPTIONB [^[\]\x00\n]* @MLKDESCRIPTIONE "]]";
        bracket_link_sim = "[["  @MLKTYPEB links_type @MLKTYPEE ":" @MLKPATHB [^[\]\x00\n]+ @MLKPATHE "]]";


        link_plain = @MLKTYPEB links_type @MLKTYPEE ":" @MLKPATHB [^\][\x20\x00\x09\x0a\n\r()<>]+ ([_0-9a-zA-Z] | "/" ) @MLKPATHE  ;

//keywords
         options_keywords = 'ARCHIVE:'| 'AUTHOR:'| 'BIND:'| 'CATEGORY:'| 'COLUMNS:'| 'CREATOR:'| 'DATE:'| 'DESCRIPTION:'| 'DRAWERS:'| 'EMAIL:'| 'EXCLUDE_TAGS:'| 'FILETAGS:'| 'INCLUDE:'| 'INDEX:'| 'KEYWORDS:'| 'LANGUAGE:'| 'MACRO:'| 'OPTIONS:'| 'PROPERTY:'| 'PRIORITIES:'| 'SELECT_TAGS:'| 'SEQ_TODO:'| 'SETUPFILE:'| 'STARTUP:'| 'TAGS:'| 'TITLE:'| 'TODO:'| 'TYP_TODO:';
         keywords = "#+" options_keywords @optkw1 char* @optkw2;


         options_keywords_ox = ('TOC' | 'HTML' | 'ASCII' | 'MD' | 'MARKDOWN' | 'BEAMER' | 'ATTR_LATEX' | 'ATTR_ASCII'| 'ATTR_BEAMER'| 'ATTR_GROFF'| 'ATTR_HTML'| 'ATTR_LATEX'| 'ATTR_MAN'| 'ATTR_ODT'| 'ATTR_TEXINFO'| 'ATTR_WRAP' | 'CINDEX' | 'FINDEX' | 'KINDEX' | 'PINDEX' | 'TINDEX' | 'VINDEX' | 'TEXINFO') ":";

         keywords_ox = "#+" options_keywords_ox @optkw1 " " char* @optkw2;

//list items
         list_item = wsp @preblank ( [-+*] @optkw1 | [0-9]{1,8}[.)]) @optkw2 [ \t];
         list_item_ordered = wsp @preblank  [0-9]+[.)] @optkw2 [ \t];
         list_counter = "[@" @MA_KEYB ([A-Za-z] | [0-9]+) @MA_KEYE "]";
         list_checkbox = @MA_EMPHE "[" [X -] "]"  ;
         list_tag = [^\x00\n\r]+ " :: " ;

//blocks
         blocks_begin = wsp @preblank '#+BEGIN' @blktype (":" | "_" ('CENTER' | 'COMMENT' | 'EXAMPLE' | 'EXPORT ' wsp @blkexp_langb [a-zA-Z0-9]+ @blkexp_lange | 'QUOTE' | 'SRC ' wsp @blksrc_langb [a-zA-Z0-9_]+ @blksrc_lange| 'VERSE')) ;
         blocks_end = wsp @preblank '#+END' @blktype ( ":" | "_" ('CENTER' | 'COMMENT' | 'EXAMPLE' | 'EXPORT' | 'QUOTE' | 'SRC' | 'VERSE')) wsp;


//footnotes
        //unicode_word = [0-9A-Z_a-z\u00AA\u00B5\u00BA\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02C1\u02C6-\u02D1\u02E0-\u02E4\u02EC\u02EE\u0300-\u0374\u0376\u0377\u037A-\u037D\u037F\u0386\u0388-\u038A\u038C\u038E-\u03A1\u03A3-\u03F5\u03F7-\u0481\u0483-\u052F\u0531-\u0556\u0559\u0561-\u0587\u0591-\u05BD\u05BF\u05C1\u05C2\u05C4\u05C5\u05C7\u05D0-\u05EA\u05F0-\u05F2\u0610-\u061A\u0620-\u0669\u066E-\u06D3\u06D5-\u06DC\u06DF-\u06E8\u06EA-\u06FC\u06FF\u0710-\u074A\u074D-\u07B1\u07C0-\u07F5\u07FA\u0800-\u082D\u0840-\u085B\u0860-\u086A\u08A0-\u08B4\u08B6-\u08BD\u08D4-\u08E1\u08E3-\u0963\u0966-\u096F\u0971-\u0983\u0985-\u098C\u098F\u0990\u0993-\u09A8\u09AA-\u09B0\u09B2\u09B6-\u09B9\u09BC-\u09C4\u09C7\u09C8\u09CB-\u09CE\u09D7\u09DC\u09DD\u09DF-\u09E3\u09E6-\u09F1\u09FC\u0A01-\u0A03\u0A05-\u0A0A\u0A0F\u0A10\u0A13-\u0A28\u0A2A-\u0A30\u0A32\u0A33\u0A35\u0A36\u0A38\u0A39\u0A3C\u0A3E-\u0A42\u0A47\u0A48\u0A4B-\u0A4D\u0A51\u0A59-\u0A5C\u0A5E\u0A66-\u0A75\u0A81-\u0A83\u0A85-\u0A8D\u0A8F-\u0A91\u0A93-\u0AA8\u0AAA-\u0AB0\u0AB2\u0AB3\u0AB5-\u0AB9\u0ABC-\u0AC5\u0AC7-\u0AC9\u0ACB-\u0ACD\u0AD0\u0AE0-\u0AE3\u0AE6-\u0AEF\u0AF9-\u0AFF\u0B01-\u0B03\u0B05-\u0B0C\u0B0F\u0B10\u0B13-\u0B28\u0B2A-\u0B30\u0B32\u0B33\u0B35-\u0B39\u0B3C-\u0B44\u0B47\u0B48\u0B4B-\u0B4D\u0B56\u0B57\u0B5C\u0B5D\u0B5F-\u0B63\u0B66-\u0B6F\u0B71\u0B82\u0B83\u0B85-\u0B8A\u0B8E-\u0B90\u0B92-\u0B95\u0B99\u0B9A\u0B9C\u0B9E\u0B9F\u0BA3\u0BA4\u0BA8-\u0BAA\u0BAE-\u0BB9\u0BBE-\u0BC2\u0BC6-\u0BC8\u0BCA-\u0BCD\u0BD0\u0BD7\u0BE6-\u0BEF\u0C00-\u0C03\u0C05-\u0C0C\u0C0E-\u0C10\u0C12-\u0C28\u0C2A-\u0C39\u0C3D-\u0C44\u0C46-\u0C48\u0C4A-\u0C4D\u0C55\u0C56\u0C58-\u0C5A\u0C60-\u0C63\u0C66-\u0C6F\u0C80-\u0C83\u0C85-\u0C8C\u0C8E-\u0C90\u0C92-\u0CA8\u0CAA-\u0CB3\u0CB5-\u0CB9\u0CBC-\u0CC4\u0CC6-\u0CC8\u0CCA-\u0CCD\u0CD5\u0CD6\u0CDE\u0CE0-\u0CE3\u0CE6-\u0CEF\u0CF1\u0CF2\u0D00-\u0D03\u0D05-\u0D0C\u0D0E-\u0D10\u0D12-\u0D44\u0D46-\u0D48\u0D4A-\u0D4E\u0D54-\u0D57\u0D5F-\u0D63\u0D66-\u0D6F\u0D7A-\u0D7F\u0D82\u0D83\u0D85-\u0D96\u0D9A-\u0DB1\u0DB3-\u0DBB\u0DBD\u0DC0-\u0DC6\u0DCA\u0DCF-\u0DD4\u0DD6\u0DD8-\u0DDF\u0DE6-\u0DEF\u0DF2\u0DF3\u0E01-\u0E3A\u0E40-\u0E4E\u0E50-\u0E59\u0E81\u0E82\u0E84\u0E87\u0E88\u0E8A\u0E8D\u0E94-\u0E97\u0E99-\u0E9F\u0EA1-\u0EA3\u0EA5\u0EA7\u0EAA\u0EAB\u0EAD-\u0EB9\u0EBB-\u0EBD\u0EC0-\u0EC4\u0EC6\u0EC8-\u0ECD\u0ED0-\u0ED9\u0EDC-\u0EDF\u0F00\u0F18\u0F19\u0F20-\u0F29\u0F35\u0F37\u0F39\u0F3E-\u0F47\u0F49-\u0F6C\u0F71-\u0F84\u0F86-\u0F97\u0F99-\u0FBC\u0FC6\u1000-\u1049\u1050-\u109D\u10A0-\u10C5\u10C7\u10CD\u10D0-\u10FA\u10FC-\u1248\u124A-\u124D\u1250-\u1256\u1258\u125A-\u125D\u1260-\u1288\u128A-\u128D\u1290-\u12B0\u12B2-\u12B5\u12B8-\u12BE\u12C0\u12C2-\u12C5\u12C8-\u12D6\u12D8-\u1310\u1312-\u1315\u1318-\u135A\u135D-\u135F\u1380-\u138F\u13A0-\u13F5\u13F8-\u13FD\u1401-\u166C\u166F-\u167F\u1681-\u169A\u16A0-\u16EA\u16EE-\u16F8\u1700-\u170C\u170E-\u1714\u1720-\u1734\u1740-\u1753\u1760-\u176C\u176E-\u1770\u1772\u1773\u1780-\u17D3\u17D7\u17DC\u17DD\u17E0-\u17E9\u180B-\u180D\u1810-\u1819\u1820-\u1877\u1880-\u18AA\u18B0-\u18F5\u1900-\u191E\u1920-\u192B\u1930-\u193B\u1946-\u196D\u1970-\u1974\u1980-\u19AB\u19B0-\u19C9\u19D0-\u19D9\u1A00-\u1A1B\u1A20-\u1A5E\u1A60-\u1A7C\u1A7F-\u1A89\u1A90-\u1A99\u1AA7\u1AB0-\u1ABE\u1B00-\u1B4B\u1B50-\u1B59\u1B6B-\u1B73\u1B80-\u1BF3\u1C00-\u1C37\u1C40-\u1C49\u1C4D-\u1C7D\u1C80-\u1C88\u1CD0-\u1CD2\u1CD4-\u1CF9\u1D00-\u1DF9\u1DFB-\u1F15\u1F18-\u1F1D\u1F20-\u1F45\u1F48-\u1F4D\u1F50-\u1F57\u1F59\u1F5B\u1F5D\u1F5F-\u1F7D\u1F80-\u1FB4\u1FB6-\u1FBC\u1FBE\u1FC2-\u1FC4\u1FC6-\u1FCC\u1FD0-\u1FD3\u1FD6-\u1FDB\u1FE0-\u1FEC\u1FF2-\u1FF4\u1FF6-\u1FFC\u203F\u2040\u2054\u2071\u207F\u2090-\u209C\u20D0-\u20F0\u2102\u2107\u210A-\u2113\u2115\u2119-\u211D\u2124\u2126\u2128\u212A-\u212D\u212F-\u2139\u213C-\u213F\u2145-\u2149\u214E\u2160-\u2188\u2C00-\u2C2E\u2C30-\u2C5E\u2C60-\u2CE4\u2CEB-\u2CF3\u2D00-\u2D25\u2D27\u2D2D\u2D30-\u2D67\u2D6F\u2D7F-\u2D96\u2DA0-\u2DA6\u2DA8-\u2DAE\u2DB0-\u2DB6\u2DB8-\u2DBE\u2DC0-\u2DC6\u2DC8-\u2DCE\u2DD0-\u2DD6\u2DD8-\u2DDE\u2DE0-\u2DFF\u2E2F\u3005-\u3007\u3021-\u302F\u3031-\u3035\u3038-\u303C\u3041-\u3096\u3099\u309A\u309D-\u309F\u30A1-\u30FA\u30FC-\u30FF\u3105-\u312E\u3131-\u318E\u31A0-\u31BA\u31F0-\u31FF\u3400-\u4DB5\u4E00-\u9FEA\uA000-\uA48C\uA4D0-\uA4FD\uA500-\uA60C\uA610-\uA62B\uA640-\uA672\uA674-\uA67D\uA67F-\uA6F1\uA717-\uA71F\uA722-\uA788\uA78B-\uA7AE\uA7B0-\uA7B7\uA7F7-\uA827\uA840-\uA873\uA880-\uA8C5\uA8D0-\uA8D9\uA8E0-\uA8F7\uA8FB\uA8FD\uA900-\uA92D\uA930-\uA953\uA960-\uA97C\uA980-\uA9C0\uA9CF-\uA9D9\uA9E0-\uA9FE\uAA00-\uAA36\uAA40-\uAA4D\uAA50-\uAA59\uAA60-\uAA76\uAA7A-\uAAC2\uAADB-\uAADD\uAAE0-\uAAEF\uAAF2-\uAAF6\uAB01-\uAB06\uAB09-\uAB0E\uAB11-\uAB16\uAB20-\uAB26\uAB28-\uAB2E\uAB30-\uAB5A\uAB5C-\uAB65\uAB70-\uABEA\uABEC\uABED\uABF0-\uABF9\uAC00-\uD7A3\uD7B0-\uD7C6\uD7CB-\uD7FB\uF900-\uFA6D\uFA70-\uFAD9\uFB00-\uFB06\uFB13-\uFB17\uFB1D-\uFB28\uFB2A-\uFB36\uFB38-\uFB3C\uFB3E\uFB40\uFB41\uFB43\uFB44\uFB46-\uFBB1\uFBD3-\uFD3D\uFD50-\uFD8F\uFD92-\uFDC7\uFDF0-\uFDFB\uFE00-\uFE0F\uFE20-\uFE2F\uFE33\uFE34\uFE4D-\uFE4F\uFE70-\uFE74\uFE76-\uFEFC\uFF10-\uFF19\uFF21-\uFF3A\uFF3F\uFF41-\uFF5A\uFF66-\uFFBE\uFFC2-\uFFC7\uFFCA-\uFFCF\uFFD2-\uFFD7\uFFDA-\uFFDC];
        unicode_word = [a-zA-Z0-9_];
        footnote_standard = "[fn:" @MA_KEYB (unicode_word )+ @MA_KEYE "]";
        footnote_definition = "[fn:" @optkw1 (unicode_word )+ @optkw2 "]";
        footnote_inline = "[fn:" @MA_KEYB (unicode_word )* @MA_KEYE ":" @MA_VALUEB ([^\x00\x20\n\r\]])+ @MA_VALUEE "]";


//header

        stars = @TIST1 start+ @TIST2 " ";
        title_kw_pr = stars wsp keyword wsp priority wsp @TITI1 title @TITI2 ;
        title_kw = stars wsp keyword wsp @TITI1 title @TITI2 ;
        title_pr = stars wsp priority wsp @TITI1 title @TITI2 ;
        title_bare = stars @TITI1 wsp title @TITI2 ;

        tskeyword = @TTKW1 ("DEADLINE:" | "SCHEDULED:" | "CLOSED:") @TTKW2;
        tswarning =  [-]{1,2} digit{1,8} [hdwmy] ;
        ts_repeater =  [+.]? "+"  digit{1,8} [hdwmy] ;
        ts_repeater_warn = (ts_repeater " " tswarning| tswarning | ts_repeater) ;
        ts_timerange = "--" @TTTRH1 [012]digit @TTTRH2 sep
                      @TTTRM1 [012345]digit @TTTRM2;
        tstime = @TTHOUR1 [012]?digit @TTHOUR2 sep
                      @TTMIN1 [012345]digit @TTMIN2 ts_timerange?;
        timestamp = @MA_VALUEB ("<" | "[") @TTYEAR1 [123]digit{3} @TTYEAR2 dsep
                    @TTMONTH1 [01]digit @TTMONTH2 dsep
                    @TTDAY1 [0123]digit @TTDAY2
                    " "+ [a-zA-Z]{3} @TTREP1
                    (" " tstime (" " ts_repeater_warn)? | " " ts_repeater_warn)?  @TTVE
                    (">" | "]") @MA_VALUEE;

         props = wsp ":" [a-zA-Z0-9_%$]+  ":" char* eol;
         properties_drawer = wsp ':PROPERTIES:' wsp eol
                             @propd_beg  props+
                             wsp ':END:' wsp @propd_end;
// Drawers
        drawer_name = ":" @optkw1 [a-zA-Z0-9-_]+ @optkw2 ":";
//lists
        statistic_cookie = "[" [0-9]{0,8} ( "%" | "/" [0-9]{0,8} ) "]";

// latex
        latex_dmath = @MA_KEYB "\\[" ( "\\" [^\]\x00\n] | [^\\\x00\n])*   "\\]" @MA_KEYE;
        latex_math = @MA_KEYB "\\(" ( "\\" [^)\x00\n] | [^\\\x00\n])* "\\)" @MA_KEYE;
        tex_math = @MA_KEYB [^$\x00\n]?"$" [^$\x00\n]+ "$" @MA_KEYE;
        tex_dmath = @MA_KEYB "$$" [^$\x00\n]+ "$"{2} @MA_KEYE;
        latex_macro_args = ("[" [^[\]{}\x00\n]* "]") | ("{" ( "\\"[a-zA-Z]+ "*"? ("[" [^[\]{}\x00\n]* "]" | "{" [^{}\x00\n]* "}")* |[^{}\x00\n])* "}") ;
        latex_macro = @MA_KEYB "\\" [a-zA-Z]+ "*"? latex_macro_args* @MA_KEYE;

        latex_begin = "\\begin{" @optkw1 [a-zA-Z0-9*]+ @optkw2 "}";
        latex_end = "\\end{" @optkw1 [a-zA-Z0-9*]+ @optkw2 "}";

// export snippets
        export_snippet = "@@" @MA_KEYB [a-zA-Z0-9]+ @MA_KEYE ":" @MA_VALUEB ( "@" [^@\x00\n] | [^@\x00\n])+ @MA_VALUEE "@@";

//entity
        entity = "\\" @MA_KEYB ("S"|"le"|"lt"|"ne"|"Xi"|"ni"|"pm"|"pi"|"lim"|"dim"|"sim"|"hom"|"det"|"lrm"|"not"|"Pi"|"lor"|"leq"|"smile"|"zeta"|"neq"|"Pr"|"ETH"|"iota"|"Zeta"|"EUR"|"star"|"Beta"|"larr"|"darr"|"harr"|"prime"|"image"|"simeq"|"propto"|"dots"|"heartsuit"|"preceq"|"Eta"|"diamondsuit"|"laquo"|"psi"|"Prime"|"para"|"cot"|"sdot"|"THORN"|"setminus"|"Psi"|"lesseqgtr"|"lsquo"|"prod"|"rarr"|"ker"|"ntilde"|"Gamma"|"Ntilde"|"hearts"|"Iota"|"ldquo"|"raquo"|"diamond"|"rlm"|"permil"|"dalet"|"perp"|"ll"|"prop"|"Diamond"|"lambda"|"cdot"|"to"|"crarr"|"rsquo"|"delta"|"diams"|"partial"|"real"|"sum"|"sad"|"rdquo"|"Delta"|"spadesuit"|"Ll"|"Atilde"|"Lambda"|"cos"|"quot"|"omega"|"odot"|"ordm"|"otimes"|"lArr"|"dArr"|"hArr"|"Kappa"|"otilde"|"spades"|"oline"|"hbar"|"hellip"|"trade"|"sbquo"|"sect"|"ast"|"parallel"|"lll"|"Uuml"|"iuml"|"Omega"|"xi"|"rArr"|"prec"|"Euml"|"beta"|"iquest"|"int"|"Yuml"|"lsaquo"|"tilde"|"cdots"|"Otilde"|"supe"|"atilde"|"uarr"|"nabla"|"Uacute"|"iacute"|"sup1"|"kappa"|"times"|"Eacute"|"limsup"|"smiley"|"preccurlyeq"|"supset"|"Yacute"|"nu"|"Nu"|"plus"|"circ"|"cap"|"isin"|"Auml"|"Mu"|"Iuml"|"rsaquo"|"cedil"|"nexist"|"cent"|"nsup"|"sup2"|"sup3"|"wedge"|"Alpha"|"alefsym"|"pound"|"bdquo"|"Ccedil"|"sube"|"nbsp"|"land"|"ouml"|"dagger"|"Aacute"|"middot"|"Iacute"|"Tau"|"loz"|"Dagger"|"frac14"|"sigma"|"subset"|"Sigma"|"ge"|"nexists"|"gt"|"lessgtr"|"sup"|"sup"|"langle"|"ddag"|"weierp"|"oacute"|"uml"|"clubsuit"|"blacksmile"|"amp"|"nsub"|"omicron"|"Rho"|"ccedil"|"AA"|"Chi"|"lceil"|"frac34"|"phi"|"Ouml"|"frac12"|"auml"|"yuml"|"geq"|"oplus"|"iexcl"|"succeq"|"uArr"|"Phi"|"rangle"|"clubs"|"alpha"|"cup"|"copy"|"gets"|"Oacute"|"rho"|"sec"|"aacute"|"yacute"|"rceil"|"Omicron"|"div"|"chi"|"triangleq"|"coth"|"uuml"|"piv"|"Theta"|"ln"|"in"|"forall"|"gamma"|"bullet"|"arccos"|"tau"|"frasl"|"sin"|"uacute"|"micro"|"macr"|"curren"|"sub"|"cosh"|"Upsilon"|"brvbar"|"plusmn"|"Epsilon"|"scaron"|"eta"|"notin"|"Scaron"|"check"|"bull"|"mu"|"checkmark"|"lg"|"Ugrave"|"igrave"|"vee"|"Gg"|"lang"|"zwj"|"Egrave"|"csc"|"vert"|"gimel"|"deg"|"deg"|"log"|"neg"|"there4"|"therefore"|"acute"|"under"|"lfloor"|"dag"|"thetasym"|"cong"|"succ"|"rang"|"radic"|"varpi"|"beth"|"theta"|"Agrave"|"reg"|"Igrave"|"minus"|"because"|"lowast"|"emsp"|"varsigma"|"sinh"|"mho"|"rfloor"|"euro"|"succcurlyeq"|"approx"|"ograve"|"ordf"|"colon"|"yen"|"hookleftarrow"|"Aring"|"tan"|"arcsin"|"Ucirc"|"icirc"|"oslash"|"asciicirc"|"arctan"|"Ecirc"|"ell"|"euml"|"Ograve"|"Uparrow"|"agrave"|"asymp"|"thinsp"|"angle"|"upsilon"|"exist"|"max"|"gcd"|"min"|"liminf"|"eacute"|"arg"|"AElig"|"vbar"|"Oslash"|"Acirc"|"Icirc"|"ugrave"|"oelig"|"aring"|"exists"|"ocirc"|"zwnj"|"sigmaf"|"acutex"|"frown"|"tanh"|"imath"|"equal"|"frowny"|"aelig"|"szlig"|"jmath"|"OElig"|"Ocirc"|"acirc"|"Rightarrow"|"emptyset"|"ensp"|"ndash"|"slash"|"shy"|"ucirc"|"rightarrow"|"exp"|"gg"|"uparrow"|"varepsilon"|"epsilon"|"ang"|"varphi"|"fnof"|"egrave"|"vartheta"|"aleph"|"thorn"|"leftrightarrow"|"leftarrow"|"Leftrightarrow"|"Leftarrow"|"equiv"|"empty"|"mdash"|"downarrow"|"ecirc"|"Downarrow"|"upsih"|"inf"|"ggg"|"infin"|"eth"|"infty") @MA_KEYE  ;

*/


#include <stdint.h>
#include <signal.h>

const unsigned char *lex_line(struct corg_ctx *ctx);


int isEND(const unsigned char *str){
    return ((str[0]=='e' || str[0]=='E') &&
            (str[1]=='n' || str[1]=='N') &&
            (str[2]=='D' || str[2]=='d'));
}

#define GDB  raise(SIGINT);
struct corg_bol_ret r0 = {0};
enum corg_bol_t whatbol(const unsigned char *YYCURSOR, struct corg_bol_ret *ret)
{
    const unsigned char *YYMARKER;
    #include <string.h>
    const unsigned char *start = YYCURSOR;
    enum corg_bol_t status = CORG_BOL_OTHER;
    *ret = r0;
    const unsigned char *blkexp_langb=0, *blkexp_lange=0,*blksrc_langb=0,*blksrc_lange=0,*optkw1=0, *optkw2=0, *blktype=0, *preblank=start;


    /*!stags:re2c format = 'const unsigned char *@@;'; */

    /*!re2c
        *   { status = CORG_BOL_OTHER;goto end; }
        end { status = CORG_BOL_EOF;goto end; }
        wsp @preblank eol { status = CORG_BOL_NEWLINE;goto end;}

        "*"+ " " wsp {
            status = CORG_BOL_HEADLINE;goto end;
        }

        list_item {
            status = CORG_BOL_LIST_ITEM;goto end;
        }

        blocks_begin {
            ret->blktype = blktype;
            if(blkexp_lange) {
                optkw2=blkexp_lange;
                optkw1=blkexp_langb;
            }else if(blksrc_lange) {
                optkw2=blksrc_lange;
                optkw1=blksrc_langb;
            }
            status = CORG_BOL_BLOCK_BEGIN;goto end;
        }

        blocks_end / (eol | end){
            ret->blktype = blktype;
            status = CORG_BOL_BLOCK_END;goto end;
        }

        wsp @preblank keywords {
            status = CORG_BOL_KEYWORD; goto end;
        }

        wsp @preblank keywords_ox {
            status = CORG_BOL_KEYWORD_OX; goto end;
        }

        wsp @preblank "#+" [^\x00\n\x20\t:]+ ":" @optkw1   char* @optkw2 {
             status = CORG_BOL_KEYWORD_CUSTOM; goto end;
        }

        wsp @preblank ":" (" " | eol) {
           status = CORG_BOL_FIXED_WIDTH;
           goto end;
        }

        wsp @preblank "# "  {
           status = CORG_BOL_COMMENT;
           goto end;
        }

        wsp @preblank "-"{5,} wsp / (eol | end) {
           status = CORG_BOL_HRULE;
           goto end;
        }
        wsp @preblank "|" [\x20\t-] wsp [^\x00\x20] {
            status = CORG_BOL_TABLE;goto end;
        }

        wsp @preblank drawer_name wsp / (end | eol){
            status = CORG_BOL_DRAWER_BEG;
            if((optkw2-optkw1)==3 && isEND(optkw1))
                status = CORG_BOL_DRAWER_END;
            goto end;
        }

        wsp @preblank latex_begin {
            status = CORG_BOL_LATEXENV_BEG;goto end;
        }
        wsp @preblank latex_end wsp / (end | eol) {
            status = CORG_BOL_LATEXENV_END;goto end;
        }

        @preblank footnote_definition wsp  {
            status = CORG_BOL_FOOTNOTE_DEF;goto end;
        }

    */
end:
    ret->pre_blank=(int)(preblank-start);
    ret->optkw2 = optkw2;
    ret->optkw1 = optkw1;
    ret->ret = status;

    return status;
}


struct markers_link mlk0 = {0};
struct ts_pointers  ts0 = {0};
struct title_pointers ti0 = {0};
void insert_new_link(struct corg_ctx *ctx,struct node **at, enum corg_link_format_t format, struct markers_link *mlkp){

    struct markers_link mlk = *mlkp;
    struct node *linkn = new_node(onLink, ctx);
    struct corg_link *link = linkn->plist.link;
    link->format = format;
    link->typet = oltFuzzy;

    struct node *emp=0;
    const unsigned char *justafter = MLKPATHE;
    if(MLKTYPEB){
        SET_STRING(link->type, MLKTYPEB, (int)(MLKTYPEE - MLKTYPEB));
        link->typet = oltSupported;
    }
    if(MLKDESCRIPTIONB){
        emp = new_leaf_node_with_text(MLKDESCRIPTIONB, (int)(MLKDESCRIPTIONE - MLKDESCRIPTIONB), ctx);
        justafter = MLKDESCRIPTIONE;
        insert_last(linkn, emp);

    }

    switch(format){
    case olfAngle: {
        justafter += 1;
        break;
    }
    case olfBracket:{
        justafter+=2;
        if(*(MLKPATHB-1) == '#')
            link->typet = oltCustom_id;
        if(*(MLKPATHB-1) == '(')
            link->typet = oltCoderef;
        break;
    }
    case olfPlain:
        assert(MLKTYPEB == (*at)->text.position + (*at)->text.len);
        break;
    }
    SET_STRING(linkn->plist.link->path, MLKPATHB, (int)(MLKPATHE - MLKPATHB));
    insert_last((*at)->parent, linkn);
    emp = new_leaf_node_with_text(justafter, 0, ctx);
    insert_last((*at)->parent, emp);
    (*at)=emp;
    assert((*at)->text.position >= MLKPATHB);
    *mlkp=mlk0;
}

void insert_new_latex_fragment(struct corg_ctx *ctx,struct node **at, struct markers ma){

    struct node * nlatex = new_node(onLatex_Fragment, ctx);

    SET_STRING(nlatex->text, MA_KEYB, (int)(MA_KEYE-MA_KEYB))

        insert_last((*at)->parent, nlatex);
    struct node *emp = new_leaf_node_with_text(MA_KEYE, 0, ctx);
    insert_last((*at)->parent, emp);
    *at=emp;

}

static const unsigned char *EMPH_CHAR = (const unsigned char *)"*_/+=~";
void pop_emph_status(struct node *emph_status[256], const unsigned char *beg){
                const unsigned char *emph_char = EMPH_CHAR;
                while(*emph_char) {
                    if(emph_status[*emph_char] && emph_status[*emph_char]->text.position > beg) {
                        emph_status[*(emph_char)]->text.len+=1; total_num+=1;
                        emph_status[*(emph_char)]->text.position--;
                        emph_status[*(emph_char)]=0;
                    }
                    emph_char++;
                }
}


void handle_emph_begin(struct corg_ctx *ctx, struct node **leafp, struct markers ma, struct node **emph_status){
#define ccursor ctx->cursor
    struct node *leaf = *leafp;
    int bytes_after = ccursor - MA_EMPHE;
    int pre = 1;
    if(MA_KEYB != MA_START && ((MA_EMPHE - MA_KEYB)==1)){
        switch(*(MA_KEYB-1)){
        case '-':
        case ' ':
        case '\t':
        case'"':
        case'{':
        case '(':
            pre = 0;
            break;
        default:
            INC_LEAF(1);
            ccursor-=bytes_after;
            return;
        }

    } else if (MA_KEYB == MA_START){
        pre =0;
    }
    if(emph_status[(int)(*(MA_EMPHE-1))] != 0) {
        INC_LEAF(pre+1);
        ccursor-=bytes_after;
        return;
    }

    INC_LEAF(pre); // emph_pre
    struct node *emp = new_leaf_node_with_text(MA_EMPHE, bytes_after, ctx);
    insert_last(leaf->parent, emp);
    *leafp=emp;
    emph_status[(int)(*(MA_EMPHE-1))] = emp;
}

void handle_emph_end(struct corg_ctx *ctx, struct node **leafp, struct markers ma, struct node **emph_status){
    struct node *leaf = *leafp;
    struct node *ne = emph_status[(int)(*(MA_EMPHB))];
    const unsigned char *e=0;
    if(ne)
        e = ne->text.position;
    assert(leaf->type == onLeaf);
    int bytes_before = MA_EMPHB-MA_BYTESCOUNT;

    if(ne && e) {
        INC_LEAF(bytes_before); total_num+=2; // 2 x markers
        struct node *node_emphasis = new_node(ctx->ascii_table[(int)(*MA_EMPHB)], ctx);
        insert_as_parent_at(ne, node_emphasis);
        node_emphasis->plist.emph->type = (*(MA_EMPHB));
        //printf("%c: %.*s\n", (int)(*(MA_EMPHB)),(int)(MA_EMPHB - e), e);
        pop_emph_status(emph_status, e);
        assert(MA_EMPHB == node_emphasis->last_child->text.position+node_emphasis->last_child->text.len);
        //ccursor--;
        //assert(MA_EMPHB+1 == ccursor);
        emph_status[(int)(*(MA_EMPHB))]=0;
        struct node *nleaf = new_leaf_node_with_text(MA_EMPHB+1, 0, ctx);
        insert_last(node_emphasis->parent, nleaf);
        *leafp=nleaf;
    } else {
        INC_LEAF(bytes_before+1);
    }

    ccursor = ccursor - (ccursor - (MA_EMPHB+1)); //captured
}

const unsigned char *lex_line(struct corg_ctx *ctx){
    const unsigned char *YYMARKER;
    const unsigned char *start=ctx->cursor;
    const unsigned char *rstart;
    struct markers_link mlk={0};
    //const unsigned char *emphb, *emphe, *keyb, *keye, *valueb, *valuee;
    struct markers ma = {0};
    MA_START = ctx->cursor;
    const unsigned char *bytes_count;struct ts_pointers ts;
    long bytes=0;
    struct node *para = NULL;
    if(ctx->document->last_child && ctx->document->last_child->type == onParagraph){
        para = ctx->document->last_child;
    }
    else {
        para = new_node(onParagraph, ctx);
        insert_last_headline(ctx, para);
    }
    struct node *leaf = new_leaf_node_with_text(ctx->cursor, 0, ctx);
    insert_last(para, leaf);
    const unsigned char *next_line = start;
    while(*next_line && *next_line!='\n') next_line++;
    struct node *emph_status[256] = {0}; // */_+=~
    /*!stags:re2c format = 'const unsigned char *@@=NULL;'; */
loop:
    MA_RSTART = ccursor;
    if(ccursor>next_line) assert(0 && "line eaten");
    /*!re2c
        re2c:define:YYCURSOR='ctx->cursor';
        re2c:yyfill:enable = 0;


        @MA_BYTESCOUNT [^\x00\n] {
            int bytes_before = ccursor-MA_BYTESCOUNT;
            INC_LEAF(bytes_before); assert(ctx->cursor - leaf->text.position - leaf->text.len <=10);
            goto loop;
        }

        end {
           //print_node(para, 0);
           //free_nodes(para);
           goto end0;
        }

        eol {
           //free_nodes(para);
           goto end0;
           //ctx->cursor--;
           //return ccursor;
        }

        @MA_EMPHB macro @MA_EMPHE {
            struct node *nm = new_node(onMacro, ctx);
            SET_STRING(nm->plist.kv->key, MA_KEYB, (int)(MA_KEYE-MA_KEYB));
            if(MA_VALUEB){
                SET_STRING(nm->plist.kv->value, MA_VALUEB+1, (int)(MA_VALUEE-MA_VALUEB-2));
            }
            struct node *emp = new_leaf_node_with_text(MA_EMPHE, 0, ctx);
            insert_last(leaf->parent, nm);
            insert_last(leaf->parent, emp);
            leaf=emp;
            goto loop;
        }

        @MA_KEYB emph_pre? [*_/+=~] @MA_EMPHE emph_border  {
           handle_emph_begin(ctx, &leaf, ma, emph_status);
           goto loop;
        }

        @MA_BYTESCOUNT emph_border @MA_EMPHB [*_/+=~]  (emph_post | eol | end) {
           handle_emph_end(ctx, &leaf, ma, emph_status);
           goto loop;
        }

        subsuperscript {
            INC_LEAF(1);
            struct node *emp = new_leaf_node_with_text(MA_VALUEB, (int)(MA_VALUEE - MA_VALUEB), ctx);
            struct node *script = new_node(*(MA_VALUEB-1) == '_' ? onSubscript: onSuperscript, ctx);
            insert_last(script, emp);
            insert_last(leaf->parent, script);
            emp = new_leaf_node_with_text(MA_VALUEE, 0, ctx);
            insert_last(leaf->parent, emp);
            leaf=emp;
           goto loop;
        }

        radio_target {
            struct node *emp = new_leaf_node_with_text(MA_VALUEB, (int)(MA_VALUEE - MA_VALUEB), ctx);
            struct node *script = new_node(onRadio_target, ctx);
            insert_last(script, emp);
            insert_last(leaf->parent, script);
            emp = new_leaf_node_with_text(MA_VALUEE+3, 0, ctx);
            insert_last(leaf->parent, emp);
            leaf=emp;
            goto loop;
        }

        target {
            struct node *emp = new_leaf_node_with_text(MA_VALUEB, (int)(MA_VALUEE - MA_VALUEB), ctx);
            struct node *script = new_node(onTarget, ctx);
            insert_last(script, emp);
            insert_last(leaf->parent, script);
            emp = new_leaf_node_with_text(MA_VALUEE+2, 0, ctx);
            insert_last(leaf->parent, emp);
            leaf=emp;
            goto loop;
        }
        timestamp {
            //printf("timestamp: %.*s\n", (int)(MA_VALUEE - MA_VALUEB), MA_VALUEB);

            struct node * ntimestamp = new_node(onTimestamp, ctx);
            int index = HEADLINE_PLANNING_SCHEDULED;
            struct timestamp *tsout = &ntimestamp->plist.timestamp->ts;
            str_to_timestamp(ts, tsout);ts=ts0;
            insert_last(leaf->parent, ntimestamp);
            struct node *emp = new_leaf_node_with_text(MA_VALUEE, 0, ctx);
            insert_last(leaf->parent, emp);
            leaf=emp;
           goto loop;
        }

        bracket_link_ext {
            insert_new_link(ctx, &leaf, olfBracket, &mlk);
            //printf("bracket_link_ext to: %.*s\n", (int)(MA_KEYE - MA_KEYB), MA_KEYB);
            //printf("bracket_link_ext ta: %.*s\n", (int)(MA_VALUEE - MA_VALUEB), MA_VALUEB);
            //struct node *emp = new_leaf_node_with_text(MA_VALUEE+2, 0, ctx);
            goto loop;
        }

        customid_link {
            insert_new_link(ctx, &leaf, olfBracket, &mlk);
            goto loop;
        }

        bracket_link_sim {
            insert_new_link(ctx, &leaf, olfBracket, &mlk);
            goto loop;
        }

        "<" link_plain ">" {
             insert_new_link(ctx, &leaf, olfAngle, &mlk);
             goto loop;
        }

        [^a-zA-Z0-9_\x00\n\r]? link_plain {
            if(MA_RSTART==MA_START || MLKTYPEB!=MA_RSTART){
                if(MLKTYPEB!=MA_RSTART) INC_LEAF(MLKTYPEB-MA_RSTART);
                insert_new_link(ctx, &leaf, olfPlain, &mlk);
            }
            else{
            INC_LEAF(1); ccursor = MA_RSTART+1;
            }
            goto loop;
        }

        export_snippet {
            struct node *nex = new_node(onExport_Snippet, ctx);
            SET_STRING(nex->plist.exp_snip->back_end, MA_KEYB, (int)(MA_KEYE-MA_KEYB));
            SET_STRING(nex->plist.exp_snip->value, MA_VALUEB, (int)(MA_VALUEE-MA_VALUEB));
            insert_last(leaf->parent, nex);
            struct node *emp = new_leaf_node_with_text(MA_VALUEE+2, 0, ctx);
            insert_last(leaf->parent, emp);
            leaf=emp;
            goto loop;
        }
        latex_dmath{
          insert_new_latex_fragment(ctx, &leaf, ma);
          goto loop;
        }
        tex_math{
          insert_new_latex_fragment(ctx, &leaf, ma);
          goto loop;
        }
        tex_dmath{
          insert_new_latex_fragment(ctx, &leaf, ma);
          goto loop;
        }
        latex_math{
          insert_new_latex_fragment(ctx, &leaf, ma);
          goto loop;
        }
        latex_macro{
          insert_new_latex_fragment(ctx, &leaf, ma);
          goto loop;
        }

        entity ([^a-zA-Z0-9] |"{}") {
            struct node *nex = new_node(onEntity, ctx);
            SET_STRING(nex->text, MA_KEYB, (int)(MA_KEYE-MA_KEYB));
            insert_last(leaf->parent, nex);
            if(*MA_KEYE!='{'){
                ccursor--;
            }
            struct node *emp = new_leaf_node_with_text(ccursor, 0, ctx);
            insert_last(leaf->parent, emp);
            leaf=emp;
            goto loop;
        }

        statistic_cookie {
            struct node *nsc = new_node(onStatistics_Cookie, ctx);
            insert_last(leaf->parent, nsc);
            SET_STRING(nsc->text, MA_RSTART, (int)(ccursor-MA_RSTART));
            struct node *emp = new_leaf_node_with_text(ccursor, 0, ctx);
            insert_last(leaf->parent, emp);
            leaf=emp;
            goto loop;
        }

        footnote_standard {
            struct node *nfn = new_node(onFootnote_Reference, ctx);
            insert_last(leaf->parent, nfn);
            nfn->plist.fn->type = FOOTNOTE_STANDARD;
            SET_STRING(nfn->plist.fn->label, MA_KEYB, (int)(MA_KEYE-MA_KEYB));
            struct node *emp = new_leaf_node_with_text(ccursor, 0, ctx);
            insert_last(leaf->parent, emp);
            leaf=emp;
            goto loop;
        }

        footnote_inline {
            struct node *nfn = new_node(onFootnote_Reference, ctx);
            insert_last(leaf->parent, nfn);
            nfn->plist.fn->type = FOOTNOTE_INLINE;
            SET_STRING(nfn->plist.fn->label, MA_KEYB, (int)(MA_KEYE-MA_KEYB));
            SET_STRING(nfn->plist.fn->value, MA_VALUEB, (int)(MA_VALUEE-MA_VALUEB));
            struct node *emp = new_leaf_node_with_text(ccursor, 0, ctx);
            insert_last(leaf->parent, emp);
            leaf=emp;
            goto loop;
        }

        "\\\\" wsp / (eol | end) {
            struct node *nsc = new_node(onLine_Break, ctx);
            insert_last(leaf->parent, nsc);
            SET_STRING(nsc->text, MA_RSTART, (int)(ccursor-MA_RSTART));
            struct node *emp = new_leaf_node_with_text(ccursor, 0, ctx);
            insert_last(leaf->parent, emp);
            leaf=emp;
            goto loop;
        }

         * {
           INC_LEAF(1);
           goto loop;
        }
*/
end0:
    pop_emph_status(emph_status, start);
    ctx->cursor--;

#undef ccursor
    UNUSED(MA_VALUEE);UNUSED(MA_VALUEB); UNUSED(yyt4);UNUSED(MA_KEYE);UNUSED(MA_KEYB);
    return 0;
}

static int header_tags(struct title_pointers *ti){ // reverse ops
    const unsigned char *ti1 = ti->ti1;
    const unsigned char **ti2 = &(ti->ti2);
    const unsigned char **tg1 = &(ti->tg1);
    const unsigned char **tg2 = &(ti->tg2);

    *tg1=0; *tg2=0;
    int nti = (int)(*ti2 - ti1);
    if(nti <= 4) return 0;
    const unsigned char *YYCURSOR = *(ti2)-1;

    static const unsigned char yybm[] = {
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,  64,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		 64,   0,   0, 128,   0, 128,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128,   0,   0,   0,   0,   0,
		128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128,   0,   0,   0,   0, 128,
		  0, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128, 128, 128, 128, 128, 128,
		128, 128, 128,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
		  0,   0,   0,   0,   0,   0,   0,   0,
    };
    int i;
    for(i=0; i<nti && (yybm[(int)*(YYCURSOR)] == 64); i++){
        YYCURSOR--;
    }
    int n_wsp_after=i;
    if(*(YYCURSOR) != ':')
        return 0;

    *tg2=YYCURSOR+1;
    YYCURSOR--;
    for(; i<nti-1 && (yybm[(int)*(YYCURSOR)] == 128); i++){
        YYCURSOR--;;
    }
    int itag = i - n_wsp_after-1;
    if(itag>=0 && (*(YYCURSOR+1) == ':')) {
        for(; i<nti-2 && (yybm[(int)*(YYCURSOR)] == 64); i++){
            if(*tg1 == NULL){
                *tg1 = YYCURSOR+1;
            }
            YYCURSOR--;
        }
        if(*tg1 != NULL) {
            int n_wsp_befor=i-n_wsp_after-itag-2;
            *ti2 = YYCURSOR + 1;
            return n_wsp_befor + itag + n_wsp_after;
        }
    }

    *tg2=0;
    return 0;
}

int parse_header(struct corg_ctx *ctx, void *header_data, process_header ph, process_planning pp, process_prop_drawer ppd)
{
    const unsigned char *YYMARKER;
    const unsigned char *YYCURSOR = ctx->cursor;
    const unsigned char *start = YYCURSOR;
    size_t len = ctx->input_string_len;
    struct markers ma = {0};
    unsigned char blocktype=0;
    const unsigned char *start_prop_drawer=NULL;
    struct ts_pointers ts;
    struct title_pointers ti;
    const unsigned char *optkw1, *propd_beg, *optkw2, *propd_end, *blktype;


    int onp_keyb, onp_keye, onp_valueb, onp_valuee;
    assert(*YYCURSOR=='*');
    /*!stags:re2c format = 'const unsigned char *@@=0;'; */


loop:
    /*!mtags:re2c format = "@@ = -1;"; */
    /*!re2c
      re2c:define:YYCURSOR='ctx->cursor';
        *   { assert("Not a header!") ; }
        end { ctx->cursor--; return 0; }
        eol { ctx->cursor--;return 0;}

        title_kw_pr {
            goto hprint;
        }
        title_kw {
            TIPR1=NULL;
            goto hprint;
        }
        title_pr {
            TIKW1=NULL;
            goto hprint;
        }
        title_bare {
            while((TITI1<TITI2) && ((*TITI1==' ') || (*TITI1=='\t'))) TITI1++;
            TIPR1=TIKW1=NULL;
            goto hprint;
        }
    */
hprint:
    blocktype = 0;
    header_tags(&ti);
    (*ph)(header_data, ti);
    goto ts;

ts:
/*!re2c
        end { ctx->cursor--; return 0; }
        * {ctx->cursor = TITI2; goto prop_drawer;}
        eol wsp (tskeyword wsp)? timestamp wsp {
            (*pp)(header_data, ts);ts=ts0;
            goto prop_drawer;}
    */
prop_drawer:
/*!re2c
        end { ctx->cursor--; return 0; }
        eol properties_drawer / (eol | end) {
            propd_end -= strlen(":END:");
            while((propd_beg<propd_end) && (*propd_end != '\n')) propd_end--;
            ppd(header_data, propd_beg, propd_end);
            goto loop;}
        * {ctx->cursor--; goto loop;}
    */
 assert(ctx->cursor <= ctx->input_string+len);
 UNUSED(MA_VALUEE);UNUSED(MA_VALUEB);UNUSED(blocktype);
 return 0;
}

int which_node_prop(const unsigned char **YYCURSOR, struct markers *res){
    struct markers ma = {0};
    const unsigned char *YYMARKER;
    int eprop=-1;
    /*!stags:re2c format = 'const unsigned char *@@=NULL;'; */
    /*!re2c
        re2c:define:YYCURSOR='*YYCURSOR';

        wsp ':END:' wsp  (end|eol) { eprop=-1; goto endnp;}

        wsp ":" @MA_KEYB 'ID' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpID; goto endnp;}
        wsp ":" @MA_KEYB 'FILE' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpFILE; goto endnp;}
        wsp ":" @MA_KEYB 'ITEM' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpITEM; goto endnp;}
        wsp ":" @MA_KEYB 'TAGS' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpTAGS; goto endnp;}
        wsp ":" @MA_KEYB 'TODO' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpTODO; goto endnp;}
        wsp ":" @MA_KEYB 'STYLE' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpSTYLE; goto endnp;}
        wsp ":" @MA_KEYB 'CLOSED' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpCLOSED; goto endnp;}
        wsp ":" @MA_KEYB 'Effort' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpEFFORT; goto endnp;}
        wsp ":" @MA_KEYB 'ALLTAGS' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpALLTAGS; goto endnp;}
        wsp ":" @MA_KEYB 'ARCHIVE' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpARCHIVE; goto endnp;}
        wsp ":" @MA_KEYB 'BLOCKED' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpBLOCKED; goto endnp;}
        wsp ":" @MA_KEYB 'COLUMNS' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpCOLUMNS; goto endnp;}
        wsp ":" @MA_KEYB 'LOGGING' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpLOGGING; goto endnp;}
        wsp ":" @MA_KEYB 'ORDERED' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpORDERED; goto endnp;}
        wsp ":" @MA_KEYB 'SUMMARY' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpSUMMARY; goto endnp;}
        wsp ":" @MA_KEYB 'CATEGORY' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpCATEGORY; goto endnp;}
        wsp ":" @MA_KEYB 'CLOCKSUM' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpCLOCKSUM; goto endnp;}
        wsp ":" @MA_KEYB 'DEADLINE' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpDEADLINE; goto endnp;}
        wsp ":" @MA_KEYB 'LOCATION' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpLOCATION; goto endnp;}
        wsp ":" @MA_KEYB 'PRIORITY' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpPRIORITY; goto endnp;}
        wsp ":" @MA_KEYB 'CUSTOM_ID' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpCUSTOM_ID; goto endnp;}
        wsp ":" @MA_KEYB 'SCHEDULED' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpSCHEDULED; goto endnp;}
        wsp ":" @MA_KEYB 'TIMESTAMP' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpTIMESTAMP; goto endnp;}
        wsp ":" @MA_KEYB 'CLOCKSUM_T' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpCLOCKSUM_T; goto endnp;}
        wsp ":" @MA_KEYB 'NOBLOCKING' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpNOBLOCKING; goto endnp;}
        wsp ":" @MA_KEYB 'UNNUMBERED' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpUNNUMBERED; goto endnp;}
        wsp ":" @MA_KEYB 'VISIBILITY' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpVISIBILITY; goto endnp;}
        wsp ":" @MA_KEYB 'COOKIE_DATA' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpCOOKIE_DATA; goto endnp;}
        wsp ":" @MA_KEYB 'DESCRIPTION' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpDESCRIPTION; goto endnp;}
        wsp ":" @MA_KEYB 'EXPORT_DATE' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpEXPORT_DATE; goto endnp;}
        wsp ":" @MA_KEYB 'EXPORT_TEXT' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpEXPORT_TEXT; goto endnp;}
        wsp ":" @MA_KEYB 'EXPORT_TITLE' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpEXPORT_TITLE; goto endnp;}
        wsp ":" @MA_KEYB 'TIMESTAMP_IA' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpTIMESTAMP_IA; goto endnp;}
        wsp ":" @MA_KEYB 'EXPORT_AUTHOR' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpEXPORT_AUTHOR; goto endnp;}
        wsp ":" @MA_KEYB 'EXPORT_OPTIONS' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpEXPORT_OPTIONS; goto endnp;}
        wsp ":" @MA_KEYB 'LOG_INTO_DRAWER' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpLOG_INTO_DRAWER; goto endnp;}
        wsp ":" @MA_KEYB 'REPEAT_TO_STATE' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpREPEAT_TO_STATE; goto endnp;}
        wsp ":" @MA_KEYB 'EXPORT_FILE_NAME' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpEXPORT_FILE_NAME; goto endnp;}
        wsp ":" @MA_KEYB 'TABLE_EXPORT_FILE' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpTABLE_EXPORT_FILE; goto endnp;}
        wsp ":" @MA_KEYB 'TABLE_EXPORT_FORMAT' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpTABLE_EXPORT_FORMAT; goto endnp;}
        wsp ":" @MA_KEYB 'CLOCK_MODELINE_TOTAL' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpCLOCK_MODELINE_TOTAL; goto endnp;}
        wsp ":" @MA_KEYB 'HTML_CONTAINER_CLASS' @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol { eprop= onpHTML_CONTAINER_CLASS; goto endnp;}
        wsp ":" @MA_KEYB [a-zA-Z0-9_%$]+  @MA_KEYE ":" wsp @MA_VALUEB char* @MA_VALUEE eol {
           eprop= onp_CUSTOM; goto endnp;
           }
        * {
           assert(0 && "unreachable");
        }
*/
 endnp:
    *res = ma;
    return eprop;
}

#include <ctype.h>

void parse_list_item(const unsigned char **YYCURSOR ,struct node *nli){
    struct markers ma = {0};
    const unsigned char *YYMARKER;
    int eprop=-1;
    MA_START = *YYCURSOR;
    /*!stags:re2c format = 'const unsigned char *@@=NULL;'; */
    /*!re2c
        re2c:define:YYCURSOR='*YYCURSOR';
        hwsp = [ \t]+;
        (list_counter hwsp) list_checkbox (hwsp|end)  {goto bdone;}
        list_checkbox (hwsp|end) {goto bdone;}
        list_counter (hwsp|end) {goto bdone;}
        * {*YYCURSOR = MA_START;goto bdone;}
    */
 bdone:
    if(*(*YYCURSOR-1) == '\0') (*YYCURSOR)--;
    nli->plist.li->counter = -1;
    if (MA_KEYB){
        char c =  toupper(MA_KEYB[0]);
        if( (c >= 'A') && (c <= 'Z')){
            nli->plist.li->counter = c - 'A' +1;
        } else {
            nli->plist.li->counter = toint(MA_KEYB, (ptrdiff_t) (MA_KEYE-MA_KEYB), 10);
        }
    }

    nli->plist.li->checkbox = CORG_LIST_CHECKBOX_NIL;
    if(MA_EMPHE){
        enum corg_list_checkbox t;
        switch(MA_EMPHE[1]){
        case ' ':{
            t = CORG_LIST_CHECKBOX_OFF;
            break;
        }
        case '-':{
            t = CORG_LIST_CHECKBOX_TRANS;
            break;
        }
        case 'X':{
            t = CORG_LIST_CHECKBOX_ON;
            break;
        }
        default:
            assert(0 && "unreachable");
        }
        nli->plist.li->checkbox = t;
    }

    if(nli->plist.li->bullet.position && !isdigit(nli->plist.li->bullet.position[0]) && **YYCURSOR){
        const unsigned char *start=*YYCURSOR;
        /*!re2c
        re2c:define:YYCURSOR='*YYCURSOR';

        list_tag {goto tdone;}
        * {*YYCURSOR = start;goto tdone;}
        */
    tdone:
        if(start != *YYCURSOR){
            SET_STRING(nli->plist.li->tag, start, (ptrdiff_t)(*YYCURSOR-start - strlen(" :: ")));
        }
    }
}
/* Local Variables: */
/* mode: c */
/* End: */
