#include <stdio.h>
#include "org_nodes.h"
#include <assert.h>

int print_header(void *data, struct title_pointers ti){
    UNUSED(data);
    assert((int)(TITI2 - TITI1) >=0);
    printf("start   :     %.*s\n", (int)(TIST2 - TIST1), TIST1);
    if(TIKW1)  {
        printf("keyword :     %.*s\n", (int)(TIKW2 - TIKW1), TIKW1);
    }
    if(TIPR1)  {
        printf("priority:     %.*s\n", (int)(TIPR2 - TIPR1), TIPR1);
    }
    printf("title   :     %.*s.\n", (int)(TITI2 - TITI1), TITI1);
    if(TITG1)  {
        printf("tags    :     %.*s\n", (int)(TITG2 - TITG1), TITG1);
    }
    return 0;
}


int print_planning(void *header_data, struct ts_pointers ts){
            printf("%c\n", *(TTYEAR1-1));
   if(TTKW1)       printf("ts.keyword     :     %.*s\n", (int)(TTKW2 - TTKW1), TTKW1);
   if(TTREP2)      printf("ts.timestamp   :     %.*s\n", (int)(TTREP2 - TTYEAR1), TTYEAR1);
   else if(ts.min2) printf("ts.timestamp   :     %.*s\n", (int)(TTMIN2 - TTYEAR1), TTYEAR1);
   else             printf("ts.timestamp   :     %.*s\n", (int)(TTDAY2 - TTYEAR1), TTYEAR1);
   UNUSED(header_data);
   return 0;
}


int print_prop_drawer(void *header_data, const unsigned char *propd_beg, const unsigned char *propd_end){
    printf("PROP DRAW: %.*s\n", (int)(propd_end - propd_beg), propd_beg);
    printf("\n");
    UNUSED(header_data);
   return 0;
}

int print_options(void *header_data, const unsigned char *YYCURSOR, const unsigned char *optkw1, const unsigned char *optkw2){
     printf("OPTIONS -- %.*s %.*s\n", (int)(optkw2 - optkw1), optkw1, (int)(YYCURSOR - optkw2), optkw2);
     UNUSED(header_data);
   return 0;
}
