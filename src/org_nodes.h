#ifndef ORG_NODES_H
#define ORG_NODES_H

#include <stdlib.h>
#include <stddef.h>

#define ARRAY_SIZE(array) sizeof(array)/sizeof(array[0])

#include "header.h"


#include "corgm.h"

#define SET_STRING(s, p, l) s.position=p;s.len=l;
#define UNUSED(x) (void)(x)

#define TRUE 1
#define FALSE 0

static const int AFFILIATED[] = { kCAPTION,
                        kDATA,
                        kHEADER,
                        kHEADERS,
                        kLABEL,
                        kNAME,
                        kPLOT,
                        kRESNAME,
                        kRESULT,
                        kRESULTS,
                        kSOURCE,
                        kSRCNAME,
                        kTBLNAME};
struct corg_mem {
  void *(*calloc)(size_t, size_t);
  void *(*realloc)(void *, size_t);
  void (*free)(void *);
} corg_mem;

#define hnFIRST 0
#define hnLAST 1
#define hnNEXT 2
#define hnPREV 3

struct node_pool{
    size_t n, m;
    struct node  *a;
};


#define CHUNK_SIZE (sizeof(struct node) * 512)
#define ALIGNMENT 8

typedef struct InternalArenaChunk {
    struct InternalArenaChunk *next;
    char data[CHUNK_SIZE];
} omt_arena_chunk;

typedef struct InternalArena {
    struct InternalArenaChunk *head;
    char *allocation_ptr;
} omt_arena;
#define USE_ARENA (ctx != NULL && (ctx->node_pool.head != NULL))

/**************************************************************************/
/* Context                                                                */
/**************************************************************************/

struct corg_ctx {
    int ascii_table[(unsigned char) -1];
    long long *node_uids;

    int copy_strings;
    int use_node_uuids;

    struct node *document;
    struct node *last_by_type[(int)(onLAST - onFIRST)+1];
    omt_arena node_pool;

    const unsigned char *input_string;
    const unsigned char *cursor;
    long line;
    size_t input_string_len;

};

#include "debug.h"
extern int total_num;
//#define DEBUG_LEN 0
#ifdef DEBUG_LEN
#define INC_LEAF(num) do{\
    log_info("total: %d num:%d ", (total_num+=num), num);\
    leaf->text.len+=num; }while(0);
#else
// log_info("text: %x %02x", *(leaf->text.position+leaf->text.len), total_num)
#define INC_LEAF(num)   do { leaf->text.len+=num;total_num+=num; } while(0);
#endif

int corg_node_check(struct node *nodes);
void insert_as_parent_at(struct node *at, struct node *node);
void insert_last(struct node *parent, struct node *child);
void insert_last_headline(struct corg_ctx *ctx, struct node *child);

struct node *new_node(enum org_node_t type, struct corg_ctx *ctx);
struct node *new_leaf_node_with_text(const unsigned char *position, int len, struct corg_ctx *ctx);

void print_node(struct node *nodes, int indent);
int ts_rpt_wrn_unit_to_seconds(char u);

#include "header.h"

int print_header(void *data, struct title_pointers ti);
int print_planning(void *header_data, struct ts_pointers ts);
int print_prop_drawer(void *header_data, const unsigned char *propd_beg, const unsigned char *propd_end);
int print_options(void *header_data, const unsigned char *YYCURSOR, const unsigned char *optkw1, const unsigned char *optkw2);


int ast_header(void *data, struct title_pointers ti);
int ast_planning(void *header_data, struct ts_pointers ts);
int ast_prop_drawer(void *header_data, const unsigned char *propd_beg, const unsigned char *propd_end);
int ast_options(void *header_data, const unsigned char *YYCURSOR, const unsigned char *optkw1, const unsigned char *optkw2);
int parse_header(struct corg_ctx *ctx, void *header_data, process_header ph, process_planning pp, process_prop_drawer ppd);
void str_to_timestamp(struct ts_pointers ts, struct timestamp *out);

const unsigned char *lex_line(struct corg_ctx *ctx);
void print_org(struct corg_ctx *ctx);
char *render_org(struct corg_ctx *ctx);

#ifdef USE_YAJL
char *render_json(struct corg_ctx *ctx);
void print_json(struct corg_ctx *ctx);
#endif

#ifdef USE_LXML
char *render_libxml(struct corg_ctx *ctx, char *filename);
#endif

// static
struct org_block_types{
    const char *name;
    const char *name_lower;
    size_t len;
    enum org_node_t node;
};
int node_t_to_obt(enum org_node_t type);
extern const struct org_block_types begin_blocks[];

struct keywords {
    const char *key;
    int hkey;
};
extern struct keywords keyword_map[][8];
enum corg_hkeys_t wkey(struct string keyword);
enum corg_hkeys_t recon_keyword(struct string keyword);

int toint(const unsigned char *str, int num, int base);
void parse_list_item(const unsigned char **YYCURSOR ,struct node *nli);
#endif
