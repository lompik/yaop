#include "org_nodes.h"
#include "scanners.h"

struct gaStr{
    size_t len;
    size_t alloc;
    char *buf;
};


struct gaStr *gaStr_new(struct corg_ctx *ctx){
    struct gaStr *res=0;
    res = malloc(sizeof(*res));
    res->len=0;
    res->buf = malloc(30);
    if(res->buf == 0) abort();
    res->alloc=30;
    memset(res->buf, 0, 30);

    UNUSED(ctx);
    return res;
}

void gaStr_free(struct gaStr *gastr){
    if(gastr->buf) free(gastr->buf);
    else log_err("%s", "Empty gaStr buf!");
    free(gastr);
}

void gaStr_ensure_enough(size_t appendedlen, struct gaStr *gastr){
    size_t newlen = gastr->len+appendedlen+1;

    if (newlen <= gastr->alloc) return ;

    if(newlen < (1024*1024))
        newlen*=2;
    else newlen+=(1024*1024);

    gastr->buf = realloc(gastr->buf, newlen);
    if(gastr->buf == NULL) abort();
    gastr->alloc = newlen;
    memset(gastr->buf+gastr->len, 0, newlen - gastr->len);
}

void gaStr_putc(char c, struct gaStr *gastr){
    gaStr_ensure_enough(1, gastr);

    gastr->buf[gastr->len] = c;
    gastr->buf[gastr->len+1] = '\0';
    gastr->len++;
}

void gaStr_cat(struct string *from, struct gaStr *to){
    if(from->len <=0 || from->position == NULL) return;
    gaStr_ensure_enough(from->len, to);
    memcpy(to->buf+to->len, from->position, from->len);
    to->len+=from->len;
}

void gaStr_puts(const char *s, struct gaStr *gastr){
    struct string str;
    str.position=(const unsigned char *)s;
    str.len = strlen(s);
    gaStr_ensure_enough(str.len, gastr);
    gaStr_cat(&str, gastr);
}

#define PUTC(x) gaStr_putc(x, gastr);
#define PUTS(x) gaStr_puts(x, gastr);


void render_node_org_recusive(struct node *node, struct gaStr *gastr){

    if(node==NULL) return;
    int child_done = FALSE ;
    switch(node->type){
    case onBold:
    case onItalic:
    case onVerbatim:
    case onUnderline:
    case onStrike_Through:{
        PUTC(node->plist.emph->type);
        render_node_org_recusive(node->first_child, gastr);
        child_done = TRUE;
        PUTC(node->plist.emph->type);
        break;
    }
    case onSubscript: {
        PUTC('_');
        break;
    }
    case onSuperscript:{
        PUTC('^');
        break;
    }
    case onSpecial_Block: {
        PUTS("#+begin:");
        if(node->text.position && node->text.len >0){
            gaStr_cat(&node->text, gastr);
        }
        PUTS("#+end:");
        break;
    }
    case onLatex_Fragment: {
        gaStr_cat(&node->text, gastr);
        break;
    }
    case onDrawer:{
        gaStr_cat(&node->text, gastr);
        break;
    }
    case onLatex_Environment:{
        gaStr_cat(&node->text, gastr);
        break;
    }
    case onExport_Snippet:{
        PUTS("@@");
        gaStr_cat(&node->plist.exp_snip->back_end, gastr);
        PUTS(":");
        gaStr_cat(&node->plist.exp_snip->value, gastr);
        PUTS("@@");
        break;
    }
    case onSrc_Block:
    case onVerse_Block:
    case onCenter_Block:
    case onExample_Block:
    case onComment_Block:
    case onExport_Block:
    case onQuote_Block: {
        PUTS("#+begin_"); PUTS(begin_blocks[node_t_to_obt(node->type)].name_lower);
        if(node->type == onSrc_Block || node->type == onExport_Block) {
            PUTC(' ');
            gaStr_cat(&node->plist.se_blocks->lang, gastr);
        }
        if(node->text.position && node->text.len >0){
            if(*node->text.position!='\n') PUTC(' ');
            gaStr_cat(&node->text, gastr);
        }
        PUTS("#+end_"); PUTS(begin_blocks[node_t_to_obt(node->type)].name_lower);
        break;
    }
    case onItem: {
        struct corg_list_item *li = node->plist.li;

        for(ptrdiff_t i=0; i< li->pre_blank; i++){
            PUTC(' ');
        }
        gaStr_cat(&li->bullet, gastr);

        if(li->counter != -1){
            char NUM[9]={0};
            sprintf(NUM, "%d", li->counter);
            PUTS("@[");
            PUTS(NUM);
            PUTS("] ")
        }
        if(li->checkbox != CORG_LIST_CHECKBOX_NIL){
            PUTC('[');
            switch(li->checkbox){
            case CORG_LIST_CHECKBOX_OFF: {PUTC(' ');break; }
            case CORG_LIST_CHECKBOX_ON: {PUTC('X');break; }
            case CORG_LIST_CHECKBOX_TRANS: {PUTC('-');break; }
            default:
                assert(0 && "unreachable");
            }
            PUTS("] ");
        }
        if(li->tag.len !=0){
            gaStr_cat(&li->tag, gastr);
            PUTS(" :: ");
        }
        break;
    }
    case onKeyword: {
        struct corg_keyword *kw = node->plist.keyword;

        PUTS("#+");
        gaStr_cat(&kw->key, gastr);

        gaStr_cat(&kw->value, gastr);
        break;
    }
    case onProperty_Drawer: {

        PUTS("\n:PROPERTIES:\n");
        render_node_org_recusive(node->first_child, gastr);
        PUTS(":END:\n");
        child_done = TRUE;
        break;
    }
    case onNode_Property: {
        PUTS(":"); gaStr_cat(&node->plist.kv->key, gastr);
        PUTS(": "); gaStr_cat(&node->plist.kv->value, gastr);
        PUTS("\n");
        break;
    }
    case onMacro: {
        PUTS("{{{"); gaStr_cat(&node->plist.kv->key, gastr);
        if(node->plist.kv->value.len != 0) {
            PUTS("("); gaStr_cat(&node->plist.kv->value, gastr); PUTS(")");
        }
        PUTS("}}}");
        break;
    }
    case onLink: {
        struct corg_link *link = node->plist.link;
        const char *beg[]={"[[", "<", ""};
        PUTS(beg[link->format]);
        gaStr_cat(&link->type, gastr);
        if(link->typet == oltCustom_id)
            PUTC('#')
        else PUTS(":");
        gaStr_cat(&link->path, gastr);
        if(link->format == olfBracket && node->first_child != 0){
            PUTS("][");
            child_done=1;
            render_node_org_recusive(node->first_child, gastr);
        }

        const char *end[]={"]]", ">", ""};
        PUTS(end[link->format]);
        break;
    }
    case onLeaf: {
        if(node->text.position && node->text.len >0){
            gaStr_cat(&node->text, gastr);
        }
        break;
    }
    case onHeadline:{
        struct corg_headline hd = *node->plist.headline;
        for(int i=0; i<hd.level; i++) PUTC('*');
        PUTC(' ');

        if(hd.todo_keyword.len > 0)  {
            gaStr_cat(&hd.todo_keyword, gastr);
            PUTC(' ');
        }
        if(hd.priority)  {
            PUTC('[');PUTC('#');
            PUTC(hd.priority);
            PUTC(']');
            PUTC(' ');
        }
        gaStr_cat(&hd.raw_value, gastr);

        if(hd.tags.len > 0)  {
            PUTC(' ');PUTC(' ');
            gaStr_cat(&hd.tags, gastr);
        }
        if(hd.planning[HEADLINE_PLANNING_DEADLINE].year_start + hd.planning[HEADLINE_PLANNING_SCHEDULED].year_start + hd.planning[HEADLINE_PLANNING_CLOSED].year_start != 0)
            PUTC('\n');
        if(hd.planning[HEADLINE_PLANNING_DEADLINE].year_start != 0){
            struct timestamp *ts = &hd.planning[HEADLINE_PLANNING_DEADLINE];
            PUTS("DEADLINE: "); gaStr_cat(&ts->raw_value, gastr);
        }
        if(hd.planning[HEADLINE_PLANNING_CLOSED].year_start != 0){
            struct timestamp *ts = &hd.planning[HEADLINE_PLANNING_CLOSED];
            PUTS("CLOSED: "); gaStr_cat(&ts->raw_value, gastr);
        }
        if(hd.planning[HEADLINE_PLANNING_SCHEDULED].year_start != 0){
            struct timestamp *ts = &hd.planning[HEADLINE_PLANNING_SCHEDULED];
            PUTS("SCHEDULED: "); gaStr_cat(&ts->raw_value, gastr);
        }
        break;
    }
    case onTimestamp:{
        struct timestamp *ts = &node->plist.timestamp->ts;
        gaStr_cat(&ts->raw_value, gastr);
        break;
    }
    case onFixed_width:{
        PUTC(':');
        gaStr_cat(&node->text, gastr);
        break;
    }
    case onComment:{
        PUTS("# ");
        gaStr_cat(&node->text, gastr);
        break;
    }
    case onHorizontal_Rule:{
        PUTS("-----");
        break;
    }
    case onStatistics_Cookie: {
        gaStr_cat(&node->text, gastr);
        break;
    }
    case onFootnote_Definition:
    case onFootnote_Reference:{
        struct corg_footnote *ftnt = node->plist.fn;
        if(ftnt->type==FOOTNOTE_DEFINITION)
            PUTS("\n");
        PUTS("[fn:");
        if(ftnt->label.position){
            gaStr_cat(&ftnt->label, gastr);
        }
        if(ftnt->value.len!=0){
            PUTS(":");
            gaStr_cat(&ftnt->value, gastr);
        }
        PUTS("]");
        break;
    }
    case onEntity: {
        PUTC('\\');
        gaStr_cat(&node->text, gastr);
        PUTS("{}");
        break;
    }
    case onLine_Break: {
        PUTC('\n');
        break;
    }

    default:
        break;
    }

    if((child_done==FALSE) && node->first_child){
        render_node_org_recusive(node->first_child, gastr);
    }

    if(node->next && (node->prev==NULL)){
        struct node *iter = node->next;
        while(iter != NULL){
            render_node_org_recusive(iter, gastr);
            iter=iter->next;
        }
    }
}



char *render_org(struct corg_ctx *ctx){
    struct gaStr *out= gaStr_new(ctx);
    gaStr_ensure_enough(ctx->input_string_len, out);
    render_node_org_recusive(ctx->document, out);
    char *res = malloc(out->len+1);
    strncpy(res,(const char *) out->buf, out->len);
    res[out->len]=0;
    gaStr_free(out);
    return res;
}

void print_org(struct corg_ctx *ctx){
    struct gaStr *out= gaStr_new(ctx);
    render_node_org_recusive(ctx->document, out);
    printf("%s", out->buf);
    gaStr_free(out);
}
