
#include "org_nodes.h"

struct node *get_node_parent(struct node *node){
    if(node != NULL) {
        return node->parent;
    }
    return NULL;
}

struct node *get_node_next(struct node *node){
    if(node != NULL) {
        return node->next;
    }
    return NULL;
}

struct node *get_node_prev(struct node *node){
    if(node != NULL) {
        return node->prev;
    }
    return NULL;
}

struct node *get_node_first_child(struct node *node){
    if(node != NULL) {
        return node->first_child;
    }
    return NULL;
}

struct node *get_node_last_child(struct node *node){
    if(node != NULL) {
        return node->last_child;
    }
    return NULL;
}

struct node *get_node_next_by_type(struct node *node){
    if(node != NULL) {
        return node->next_by_type;
    }
    return NULL;
}

struct node *get_node_prev_by_type(struct node *node){
    if(node != NULL) {
        return node->prev_by_type;
    }
    return NULL;
}

char get_plist_emph_type(struct corg_plist_emph *in){
    char res  = 0;
    if(in != NULL) {
        res = in->type;
    }
    return res;
}

struct corg_plist_emph *get_node_plist_emph(struct node *node){
    if(node != NULL) {
        if((node->type == onBold) || (node->type == onVerbatim) || (node->type == onItalic) || (node->type == onStrike_Through) || (node->type == onUnderline))
            return node->plist.emph;
    }
    return NULL;
}
struct corg_common get_headline_bytes(struct corg_headline *in){
    struct corg_common res  = {0};
    if(in != NULL) {
        res = in->bytes;
    }
    return res;
}

struct corg_headline *get_node_headline(struct node *node){
    if(node != NULL) {
        if(node->type == onHeadline)
            return node->plist.headline;
    }
    return NULL;
}
struct corg_common get_timestamp_bytes(struct corg_timestamp *in){
    struct corg_common res  = {0};
    if(in != NULL) {
        res = in->bytes;
    }
    return res;
}

struct corg_timestamp *get_node_timestamp(struct node *node){
    if(node != NULL) {
        if(node->type == onTimestamp)
            return node->plist.timestamp;
    }
    return NULL;
}
struct corg_common get_link_bytes(struct corg_link *in){
    struct corg_common res  = {0};
    if(in != NULL) {
        res = in->bytes;
    }
    return res;
}

struct corg_link *get_node_link(struct node *node){
    if(node != NULL) {
        if(node->type == onLink)
            return node->plist.link;
    }
    return NULL;
}
struct corg_common get_keyword_bytes(struct corg_keyword *in){
    struct corg_common res  = {0};
    if(in != NULL) {
        res = in->bytes;
    }
    return res;
}

struct corg_keyword *get_node_keyword(struct node *node){
    if(node != NULL) {
        if(node->type == onKeyword)
            return node->plist.keyword;
    }
    return NULL;
}
struct corg_common get_se_block_bytes(struct corg_se_block *in){
    struct corg_common res  = {0};
    if(in != NULL) {
        res = in->bytes;
    }
    return res;
}

struct corg_se_block *get_node_se_block(struct node *node){
    if(node != NULL) {
        if((node->type == onSrc_Block) || (node->type == onVerse_Block) || (node->type == onCenter_Block) || (node->type == onExample_Block) || (node->type == onComment_Block))
            return node->plist.se_blocks;
    }
    return NULL;
}
struct string get_ltxenv_environment(struct corg_ltxenv *in){
    struct string res  = {0};
    if(in != NULL) {
        res = in->environment;
    }
    return res;
}

struct corg_ltxenv *get_node_ltxenv(struct node *node){
    if(node != NULL) {
        if(node->type == onLatex_Environment)
            return node->plist.latex_env;
    }
    return NULL;
}
struct corg_common get_export_snippet_bytes(struct corg_export_snippet *in){
    struct corg_common res  = {0};
    if(in != NULL) {
        res = in->bytes;
    }
    return res;
}

struct corg_export_snippet *get_node_export_snippet(struct node *node){
    if(node != NULL) {
        if(node->type == onExport_Snippet)
            return node->plist.exp_snip;
    }
    return NULL;
}
struct corg_common get_kv_bytes(struct corg_kv *in){
    struct corg_common res  = {0};
    if(in != NULL) {
        res = in->bytes;
    }
    return res;
}

struct corg_kv *get_node_kv(struct node *node){
    if(node != NULL) {
        if((node->type == onKeyword) || (node->type == onMacro) || (node->type == onNode_Property))
            return node->plist.kv;
    }
    return NULL;
}
struct corg_common get_footnote_bytes(struct corg_footnote *in){
    struct corg_common res  = {0};
    if(in != NULL) {
        res = in->bytes;
    }
    return res;
}

struct corg_footnote *get_node_footnote(struct node *node){
    if(node != NULL) {
        if((node->type == onFootnote_Definition) || (node->type == onFootnote_Reference))
            return node->plist.fn;
    }
    return NULL;
}
enum corg_list_checkbox get_list_item_checkbox(struct corg_list_item *in){
    enum corg_list_checkbox res  = CORG_LIST_CHECKBOX_NIL;
    if(in != NULL) {
        res = in->checkbox;
    }
    return res;
}

struct corg_list_item *get_node_list_item(struct node *node){
    if(node != NULL) {
        if(node->type == onItem)
            return node->plist.li;
    }
    return NULL;
}
