#include "org_nodes.h"
#include "scanners.h"

#include <libxml/encoding.h>
#include <libxml/xmlwriter.h>


#define lxml_stringattrlit(attr, lit)do { xmlTextWriterWriteAttribute(*w, (const xmlChar *)(attr), (const xmlChar *)lit); } while(0)
#define lxml_charpattr(attr, charp, len)do { xmlChar *content = xmlCharStrndup((const char *)charp, (int) len); \
        xmlTextWriterWriteAttribute(*w, (const xmlChar *)(attr), content); xmlFree(content); } while(0)
#define lxml_stringattr(attr, s) lxml_charpattr(attr, s.position, (int)s.len)
#define lxml_stringtxt(txt)do { xmlChar *text = xmlCharStrndup((const char*)txt.position, txt.len); \
                xmlTextWriterWriteString(*w, (const xmlChar *)text); xmlFree(text); } while(0)

void render_node_libxml_recusive(struct node *node, xmlTextWriterPtr *w){

    int child_done = FALSE ;

    xmlTextWriterStartElement(*w, (const xmlChar *) org_node_t_str[node->type]+2);


    switch(node->type){
    case onBold:
    case onItalic:
    case onVerbatim:
    case onUnderline:
    case onStrike_Through:{
        //PUTS(",");
        lxml_charpattr("type", &node->plist.emph->type, 1);
        render_node_libxml_recusive(node->first_child, w);
        child_done = TRUE;

        break;
    }
    case onSubscript: {
        //PUTC('_');
        break;
    }
    case onSuperscript:{
        //PUTC('^');
        break;
    }
    case onSpecial_Block: {
        if(node->text.position && node->text.len >0){

            lxml_stringtxt(node->text);
        }

        break;
    }
    case onLatex_Fragment:{
        lxml_stringtxt(node->text);
        break;
    }
    case onLatex_Environment:{
        lxml_stringtxt(node->text);
        break;
    }
    case onDrawer:{
        lxml_stringtxt(node->text);
        break;
    }
    case onExport_Snippet:{
        lxml_stringattr("back_end", node->plist.exp_snip->back_end);
        lxml_stringattr("value", node->plist.exp_snip->value);
        break;
    }
    case onSrc_Block:
    case onVerse_Block:
    case onCenter_Block:
    case onExample_Block:
    case onComment_Block:
    case onExport_Block:
    case onQuote_Block:{
        //PUTS("#+begin_"); PUTS(begin_blocks[node_t_to_obt(node->type)].name_lower);

        if(node->type == onSrc_Block || node->type == onExport_Block) {
            lxml_stringattr("lang", node->plist.se_blocks->lang);

        }
        if(node->text.position && node->text.len >0){
            lxml_stringtxt(node->text);
        }
        break;
    }
    case onItem:{
        struct corg_list_item *li = node->plist.li;

        xmlTextWriterWriteFormatAttribute (*w,
                                           (const xmlChar *) "preblank",
                                           "%ld",
                                           li->pre_blank);

        lxml_stringattr("bullet", li->bullet);

        if(li->counter != -1){
            xmlTextWriterWriteFormatAttribute (*w,
                                               (const xmlChar *) "counter",
                                           "%d",
                                           li->counter);
        }
        if(li->checkbox != CORG_LIST_CHECKBOX_NIL){
            switch(li->checkbox){
            case CORG_LIST_CHECKBOX_OFF: {
                lxml_stringattrlit("checkbox", " ");
                break;
            }
            case CORG_LIST_CHECKBOX_ON: {
                lxml_stringattrlit("checkbox", "X");
                break;
            }
            case CORG_LIST_CHECKBOX_TRANS: {
                lxml_stringattrlit("checkbox", "-");
                break;
            }
            default:
                assert(0 && "unreachable");
            }
        }
        if(li->tag.len !=0){
            lxml_stringattr("tag", li->tag);
        }
        break;
    }
    case onKeyword: {
        struct corg_keyword *kw = node->plist.keyword;

        lxml_stringattr("key", kw->key);
        lxml_stringattr("value", kw->value);
        break;
    }
    case onProperty_Drawer: {
        break;
    }
    case onNode_Property: {

        lxml_stringattr("key", node->plist.kv->key);
        lxml_stringattr("value", node->plist.kv->value);
        break;
    }
    case onMacro: {

        lxml_stringattr("key", node->plist.kv->key);
        if(node->plist.kv->value.len != 0){
            lxml_stringattr("value", node->plist.kv->value);
        }
        break;
    }

    case onLink: {
        struct corg_link *link = node->plist.link;
        const char *beg[]={"[[", "<", ""};

        lxml_charpattr("format", beg[link->format], strlen(beg[link->format]));

        lxml_stringattr("type", link->type);
        lxml_stringattr("path", link->path);

        if(link->format == olfBracket && node->first_child != 0){
            render_node_libxml_recusive(node->first_child, w);
            child_done = TRUE;
        }

        break;
    }
    case onLeaf: {
        if(node->text.position && node->text.len >0){
            lxml_stringtxt(node->text);
        }
        break;
    }
    case onHeadline:{
        struct corg_headline hd = *node->plist.headline;
        //for(int i=0; i<hd.level; i++) PUTC('*');

        xmlTextWriterWriteFormatAttribute (*w,
                                           (const xmlChar*) "level",
                                           "%d",
                                           hd.level);




        if(hd.todo_keyword.len > 0)  {
            lxml_stringattr("todo_keyword", hd.todo_keyword);
        }
        if(hd.priority)  {
            lxml_charpattr("priority", &hd.priority, 1);
        }
        lxml_stringtxt(hd.raw_value);

        if(hd.tags.len > 0)  {
            lxml_stringattr("tags", hd.tags);
        }
        if(hd.planning[HEADLINE_PLANNING_DEADLINE].year_start + hd.planning[HEADLINE_PLANNING_SCHEDULED].year_start + hd.planning[HEADLINE_PLANNING_CLOSED].year_start != 0) {

            if(hd.planning[HEADLINE_PLANNING_DEADLINE].year_start != 0){
                struct timestamp *ts = &hd.planning[HEADLINE_PLANNING_DEADLINE];
                lxml_stringattr("planning-deadline", ts->raw_value);
            }
            if(hd.planning[HEADLINE_PLANNING_SCHEDULED].year_start != 0){
                struct timestamp *ts = &hd.planning[HEADLINE_PLANNING_SCHEDULED];
                lxml_stringattr("planning-scheduled", ts->raw_value);
            }
            if(hd.planning[HEADLINE_PLANNING_CLOSED].year_start != 0){
                struct timestamp *ts = &hd.planning[HEADLINE_PLANNING_CLOSED];
                lxml_stringattr("planning-closed", ts->raw_value);
            }

        }
        break;
    }
    case onTimestamp:{
        struct timestamp *ts = &node->plist.timestamp->ts;
        lxml_stringtxt(ts->raw_value);
        break;
    }
    case onFixed_width:{
        lxml_stringtxt(node->text);
        break;
    }
    case onComment:{
        lxml_stringtxt(node->text);
        break;
    }
    case onHorizontal_Rule:{
        //PUTS("-----");
        break;
    }
    case onStatistics_Cookie: {
        lxml_stringtxt(node->text);
        break;
    }
    case onFootnote_Definition:
    case onFootnote_Reference:{
        struct corg_footnote *ftnt = node->plist.fn;
        if(ftnt->label.position){
            lxml_stringattr("label", ftnt->label);
        }
        if(ftnt->value.len!=0){
            lxml_stringattr("value", ftnt->value);
        }

        break;
    }
    case onEntity: {
        lxml_stringtxt(node->text);
        break;
    }
    case onLine_Break: {
        //PUTC('\n');
        break;
    }

    default:
        break;
    }

    if((child_done==FALSE) && node->first_child){
        render_node_libxml_recusive(node->first_child, w);
    }

    xmlTextWriterEndElement(*w);

    if(node->next && (node->prev==NULL)){
        struct node *iter = node->next;
        while(iter != NULL){
            render_node_libxml_recusive(iter, w);
            iter=iter->next;
        }
    }
}


#include <string.h>
const char *render_libxml(struct corg_ctx *ctx, char *filename){

    int rc;
    xmlTextWriterPtr writer;

    /* Create a new XmlWriter for uri, with no compression. */
    writer = xmlNewTextWriterFilename(filename, 0);
    if (writer == NULL) {
        printf("testXmlwriterFilename: Error creating the xml writer\n");
        return NULL;
    }

    /* Start the document with the xml default for the version,
     * encoding ISO 8859-1 and the default for the standalone
     * declaration. */
    rc = xmlTextWriterStartDocument(writer, NULL, "utf-8", NULL);
    if (rc < 0) {
        printf
            ("testXmlwriterFilename: Error at xmlTextWriterStartDocument\n");
        return NULL;
    }

    render_node_libxml_recusive(ctx->document, &writer);

    rc = xmlTextWriterEndDocument(writer);
    if (rc < 0) {
        printf
            ("testXmlwriterFilename: Error at xmlTextWriterEndDocument\n");
    }

    xmlFreeTextWriter(writer);


    return "";
}

/* void print_json(struct corg_ctx *ctx){ */
/*     char *out = render_json(ctx); */
/*     printf("%s", out); */
/*     free(out); */
/* } */


/* Local Variables: */
/* company-clang-arguments: ("-I/usr/include/libxml2") */
/* flycheck-clang-include-path: ("/usr/include/libxml2") */
/* flycheck-gcc-include-path: ("/usr/include/libxml2") */
/* End: */
