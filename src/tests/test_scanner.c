#include "minunit.h"
#include "../scanners.h"
#include "../org_nodes.h"

int tests_run         = 0;
int tests_failed      = 0;
int assertions_run    = 0;
int assertions_failed = 0;

#define ARRAY_SIZE(array) sizeof(array)/sizeof(array[0])

struct {
    char id; enum org_node_t type;
} emph[] = {{'*', onBold}, {'/', onItalic}, {'=', onVerbatim}, {'~', onVerbatim}, {'+', onStrike_Through}};

#define TSTSTRMAX 100

static const char * test_list_item() {
    unsigned char *str = malloc(TSTSTRMAX);
    const unsigned char *cursor;
    struct node nli;
    nli.plist.li = malloc(sizeof(*nli.plist.li));

    sprintf((char *)str, "%s", "[@15] [X]");
    cursor = str;
    memset(nli.plist.li, 0, sizeof *nli.plist.li);
    parse_list_item(&cursor, &nli);
    mu_assert_corg_d("list counter [@digit]", nli.plist.li->counter == 15);
    mu_assert_corg_d("list checkbox [X]", nli.plist.li->checkbox == CORG_LIST_CHECKBOX_ON);

    sprintf((char *)str, "%s", "[@J] [ ]");
    cursor = str;
    memset(nli.plist.li, 0, sizeof *nli.plist.li);
    parse_list_item(&cursor, &nli);
    mu_assert_corg_d("list counter [@J]", nli.plist.li->counter == 'J'- 'A'+1);
    mu_assert_corg_d("list checkbox [ ]", nli.plist.li->checkbox == CORG_LIST_CHECKBOX_OFF);

    sprintf((char *)str, "%s", "[-]");
    cursor = str;
    memset(nli.plist.li, 0, sizeof *nli.plist.li);
    parse_list_item(&cursor, &nli);
    mu_assert_corg_d("list checkbox [-]", nli.plist.li->checkbox == CORG_LIST_CHECKBOX_TRANS);

    sprintf((char *)str, "%s", "[-]");
    cursor = str;
    memset(nli.plist.li, 0, sizeof *nli.plist.li);
    parse_list_item(&cursor, &nli);
    mu_assert_corg_d("list checkbox [-]", nli.plist.li->checkbox == CORG_LIST_CHECKBOX_TRANS);

    sprintf((char *)str, "%s", "* [-] blah sds :: here");
    cursor = str+2;
    memset(nli.plist.li, 0, sizeof *nli.plist.li);
    nli.plist.li->bullet.position = str; nli.plist.li->bullet.len = 2;
    parse_list_item(&cursor, &nli);
    mu_assert_corg_d("list cursor ", cursor[0] == 'h');
    mu_assert_corg_d("list checkbox [-]", nli.plist.li->checkbox == CORG_LIST_CHECKBOX_TRANS);
    mu_assert_corg_d("list tag ", nli.plist.li->tag.position == &str[6] && nli.plist.li->tag.len == 8);

    sprintf((char *)str, "%s", "+ [] blah\n  :: sd");
    cursor = str+2;
    memset(nli.plist.li, 0, sizeof *nli.plist.li);
    nli.plist.li->bullet.position = str; nli.plist.li->bullet.len = 2;
    parse_list_item(&cursor, &nli);
    mu_assert_corg_d("not list checkbox []", nli.plist.li->checkbox == CORG_LIST_CHECKBOX_NIL);
    mu_assert_corg_d("not list tag ", nli.plist.li->tag.position == NULL);

    free(nli.plist.li);
    free(str);
    return 0;
}

// ================================================================
static const char * all_tests() {
    //mu_run_test(test1);
    mu_run_test(test_list_item);
    return 0;
}

int main(int argc, char **argv) {

    printf("TEST_ROUNDTRIP ENTER\n");
    const char *result = all_tests();
    printf("\n");
    if (result != 0) {
        printf("Not all unit tests passed\n");
    }
    else {
        printf("TEST_ROUNDTRIP: ALL UNIT TESTS PASSED\n");
    }
    printf("Tests      passed: %d of %d\n", tests_run - tests_failed, tests_run);
    printf("Assertions passed: %d of %d\n", assertions_run - assertions_failed, assertions_run);

    (void)argc;(void)argv;
    return result != 0;
}
