#include "minunit.h"
#include "../scanners.h"
#include "../org_nodes.h"

int tests_run         = 0;
int tests_failed      = 0;
int assertions_run    = 0;
int assertions_failed = 0;


struct {
    char id; enum org_node_t type;
} emph[] = {{'*', onBold}, {'/', onItalic}, {'=', onVerbatim}, {'~', onVerbatim}, {'+', onStrike_Through}};

#define TSTSTRMAX 100
static const char * test_emphasis() {
    char *str = malloc(TSTSTRMAX);

    for(size_t i=0; i<ARRAY_SIZE(emph); i++){
        snprintf(str, TSTSTRMAX, " %c%s%c ", emph[i].id, "s  sd adasdas", emph[i].id);
        struct corg_ctx *ctx= corg_parse_to_ast((char *)str);
        mu_assert_corg(ctx->document->first_child->first_child->next->type == emph[i].type);
        free_nodes(ctx->document, ctx);
        free(ctx);
    }

    for(size_t i=0; i<ARRAY_SIZE(emph)-1; i++){
        snprintf(str, TSTSTRMAX, " %c%s %c%s%c %s%c ", emph[i].id, "Curabitur",
                 emph[i+1].id, "lacinia pulvinar", emph[i+1].id, "nibh", emph[i].id);
        struct corg_ctx *ctx= corg_parse_to_ast((char *)str);
        mu_assert_corg_d("emphasis nesting", ctx->document->first_child->first_child->next->type == emph[i].type);
        free_nodes(ctx->document, ctx);
        free(ctx);

        snprintf(str, TSTSTRMAX, " %c%s %c%s%c %s%c ", emph[i].id, "Curabitur",
                 emph[i+1].id, "lacinia pulvinar", emph[i].id, "nibh", emph[i+1].id);
        ctx= corg_parse_to_ast((char *)str);
        mu_assert_corg_d("emphasis can nest but can't cross border (outside)", ctx->document->first_child->first_child->next->type == emph[i].type);
        mu_assert_corg_d("emphasis can nest but can't cross border (inside)", ctx->document->first_child->first_child->next->next->type == onLeaf);
        free_nodes(ctx->document, ctx);
        free(ctx);
    }

    free(str);
    return 0;
}

// ================================================================
static const char * all_tests() {
    //mu_run_test(test1);
    mu_run_test(test_emphasis);
    return 0;
}

int main(int argc, char **argv) {

    printf("TEST_ARGPARSE ENTER\n");
    const char *result = all_tests();
    printf("\n");
    if (result != 0) {
        printf("Not all unit tests passed\n");
    }
    else {
        printf("TEST_ARGPARSE: ALL UNIT TESTS PASSED\n");
    }
    printf("Tests      passed: %d of %d\n", tests_run - tests_failed, tests_run);
    printf("Assertions passed: %d of %d\n", assertions_run - assertions_failed, assertions_run);

    (void)argc;(void)argv;
    return result != 0;
}
