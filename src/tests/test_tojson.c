#include "minunit.h"
#include "../scanners.h"
#define USE_YAJL
#include "../org_nodes.h"

int tests_run         = 0;
int tests_failed      = 0;
int assertions_run    = 0;
int assertions_failed = 0;

#define ARRAY_SIZE(array) sizeof(array)/sizeof(array[0])

struct {
    char id; enum org_node_t type;
} emph[] = {{'*', onBold}, {'/', onItalic}, {'=', onVerbatim}, {'~', onVerbatim}, {'+', onStrike_Through}};

#define TSTSTRMAX 100
static const char * test_emph() {

    const char *in = " *Nullam libero mauris* ";
    struct corg_ctx *ctx= corg_parse_to_ast((char *)in);
    char *out =render_json(ctx);
    print_json(ctx);
    mu_assert_corg_d("bold", strcmp(out, in) != 0);
    free_nodes(ctx->document, ctx);
    free(out);
    free(ctx);

    return 0;
}

/* static char * test_header() { */
/*     char *str = malloc(TSTSTRMAX); */
/*     char *in = "* TODO [A] item 1  :TAGS:"; */
/*     struct corg_ctx *ctx= corg_parse_to_ast((char *)in); */
/*     char *out =render_json(ctx); */
/*     mu_assert_corg_d("headline with todo, prio, tags", strcmp(out, in) == 0); */
/*     free_nodes(ctx->document, ctx); */
/*     free(out); */
/*     free(ctx); */
/*     free(str); */
/*     return 0; */
/* } */

/* static char * test_blocks() { */
/*     char *str = malloc(TSTSTRMAX); */
/*     const struct org_block_types *b=0; */
/*     for(b=begin_blocks; b->name != NULL; b++){ */
/*         snprintf(str, TSTSTRMAX, "#+begin_%s test\n%s\n#+end_%s\n", b->name, "blah blah", b->name_lower); */
/*         struct corg_ctx *ctx= corg_parse_to_ast((char *)str); */
/*         snprintf(str, TSTSTRMAX, "#+begin_%s test\n%s\n#+end_%s\n", b->name_lower, "blah blah", b->name_lower); */
/*         char *out =render_json(ctx); */
/*         mu_assert_corg_d("basic block parsing", strcmp(out, str) == 0); */
/*         free(out); */
/*         free_nodes(ctx->document, ctx); */
/*         free(ctx); */
/*     } */

/*     free(str); */
/*     return 0; */
/* } */

/* static char * test_links() { */
/*     char *str = malloc(TSTSTRMAX); */

/*     char *link_beg[]={"[[", "<", ""}; */
/*     char *link_end[]={"]]", ">", ""}; */

/*     for(size_t i=0; i<ARRAY_SIZE(link_beg); i++){ */
/*         snprintf(str, TSTSTRMAX, "%s%s:%s%s", link_beg[i], "http", "//example.com", link_end[i]); */
/*         struct corg_ctx *ctx= corg_parse_to_ast((char *)str); */
/*         char *out =render_json(ctx); */
/*         mu_assert_corg_d("basic link parsing", strcmp(out, str) == 0); */
/*         free(out); */
/*         free_nodes(ctx->document, ctx); */
/*         free(ctx); */
/*     } */

/*     { */
/*         snprintf(str, TSTSTRMAX, "[[%s:%s][%s]]", "http", "//example.com", "exa m ple"); */
/*         struct corg_ctx *ctx= corg_parse_to_ast((char *)str); */
/*         char *out =render_json(ctx); */
/*         mu_assert_corg_d("basic link with desc parsing", strcmp(out, str) == 0); */
/*         free(out); */
/*         free_nodes(ctx->document, ctx); */
/*         free(ctx); */
/*     } */

/*     free(str); */
/*     return 0; */
/* } */

/* static char * test_keywords() { */
/*     char *str = malloc(TSTSTRMAX); */

/*     { */
/*         snprintf(str, TSTSTRMAX, "#+%s blaba sd *s*", "TYP_TODO:"); */
/*         struct corg_ctx *ctx= corg_parse_to_ast((char *)str); */
/*         char *out =render_json(ctx); */
/*         mu_assert_corg_d("basic link with desc parsing", strcmp(out, str) == 0); */
/*         free(out); */
/*         free_nodes(ctx->document, ctx); */
/*         free(ctx); */
/*     } */

/*     free(str); */
/*     return 0; */
/* } */

/* #define roundtrip(msg, str) do { \ */
/*     struct corg_ctx *ctx= corg_parse_to_ast((char *)str); \ */
/*     char *out =render_json(ctx); \ */
/*     mu_assert_corg_d(msg, strcmp(out, str) == 0); \ */
/*     free(out); \ */
/*     free_nodes(ctx->document, ctx);\ */
/*     free(ctx); \ */
/* } while(0) */

/* static char * test_linebreak() { */
/*     char *str = malloc(TSTSTRMAX); */
/*     snprintf(str, TSTSTRMAX, "abc \n as d\n\n as d\n"); */
/*     roundtrip("basic linebreak", str); */
/*     free(str); */
/*     return 0; */
/* } */

// ================================================================
static const char * all_tests() {
    //mu_run_test(test1);
//    mu_run_test(test_header);
    mu_run_test(test_emph);
    //  mu_run_test(test_blocks);
    //mu_run_test(test_links);
    //mu_run_test(test_linebreak);
    //mu_run_test(test_hrule);
    return 0;
}

int main(int argc, char **argv) {

    printf("TEST_ROUNDTRIP ENTER\n");
    const char *result = all_tests();
    printf("\n");
    if (result != 0) {
        printf("Not all unit tests passed\n");
    }
    else {
        printf("TEST_ROUNDTRIP: ALL UNIT TESTS PASSED\n");
    }
    printf("Tests      passed: %d of %d\n", tests_run - tests_failed, tests_run);
    printf("Assertions passed: %d of %d\n", assertions_run - assertions_failed, assertions_run);

    (void)argc;(void)argv;
    return result != 0;
}
